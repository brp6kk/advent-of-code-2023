'''https://adventofcode.com/2023/day/22'''

import utils
from copy import deepcopy

class Point():
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
    
    def __eq__(self, other):
        return self.x == other.x and self.y == other.y and self.z == other.z

    def __hash__(self):
        return hash((self.x, self.y, self.z))

class Brick():
    def __init__(self, point1, point2):
        self.point1 = point1
        self.point2 = point2

    def move_down(self, num_spaces):
        self.point1.z -= num_spaces
        self.point2.z -= num_spaces
    
    def min_z(self):
        return min(self.point1.z, self.point2.z)
    
    def max_z(self):
        return max(self.point1.z, self.point2.z)
    
    def all_x_y_coords(self):
        if self.point1.x == self.point2.x:
            return [(self.point1.x, y) for y in range(min(self.point1.y, self.point2.y), max(self.point1.y, self.point2.y) + 1)]
        else: # Expect self.point1.y == self.point2.y
            return [(x, self.point1.y) for x in range(min(self.point1.x, self.point2.x), max(self.point1.x, self.point2.x) + 1)]
    
    def __eq__(self, other):
        return self.point1 == other.point1 and self.point2 == other.point2
    
    def __hash__(self):
        return hash((self.point1, self.point2))

def get_bricks():
    '''Read input file & store in list of Brick objects'''
    lines = utils.get_lines("inputs/22.txt")
    bricks = []
    for line in lines:
        str_point1, str_point2 = line.strip().split("~")
        x1, y1, z1 = [int(val) for val in str_point1.split(",")]
        x2, y2, z2 = [int(val) for val in str_point2.split(",")]
        new_brick = Brick(Point(x1, y1, z1), Point(x2, y2, z2))
        bricks.append(new_brick)
    return bricks

def simulate_fall(bricks):
    '''Returns (number of bricks that fall during this simulation, bricks whose removal would cause additional falls)'''
    sorted_bricks = sorted(bricks, key = lambda brick : brick.min_z())

    # top_down_bricks[(x, y)] = list of bricks that exist at (x, y), sorted by z
    top_down_bricks = dict()
    # Bricks whose removal would cause additional falls
    unsafe_bricks = set()
    fallen_brick_count = 0

    for brick in sorted_bricks:
        # Find the highest z value of any brick below this brick
        max_z = 1
        for (x, y) in brick.all_x_y_coords():
            if (x, y) in top_down_bricks.keys():
                z = top_down_bricks[(x, y)][-1].max_z() + 1
                if z > max_z:
                    max_z = z
        
        # Move this brick down
        dz = brick.min_z() - max_z
        if dz > 0:
            brick.point1.z -= dz
            brick.point2.z -= dz
            fallen_brick_count += 1

        # For this brick, determine which bricks it lays directly on top of
        supporting_bricks = set()
        for xy in brick.all_x_y_coords():
            if xy in top_down_bricks.keys():
                top_down_bricks[xy].append(brick)
                if top_down_bricks[xy][-2].max_z() == brick.min_z() - 1:
                    supporting_bricks.add(top_down_bricks[xy][-2])
            else:
                top_down_bricks[xy] = [brick]

        if len(supporting_bricks) == 1:
            unsafe_bricks.add(supporting_bricks.pop())

    return (fallen_brick_count, unsafe_bricks)

def part1():
    bricks = get_bricks()
    _, unsafe_bricks = simulate_fall(bricks)
    print(len(bricks) - len(unsafe_bricks))
    # Time: 18:11:48
    # Rank: 10584

def part2():
    bricks = get_bricks()
    _, unsafe_bricks = simulate_fall(bricks)

    total = 0
    for brick in unsafe_bricks:
        bricks_copy = deepcopy(bricks)
        bricks_copy.remove(brick)
        count, _ = simulate_fall(bricks_copy)
        total += count

    print(total)
    # Time: 18:57:11
    # Rank: 9666

if __name__ == '__main__':
    part1()
    part2()