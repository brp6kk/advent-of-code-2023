'''https://adventofcode.com/2023/day/7'''

import utils

class HandInfo():
    def __init__(self, hand, distinct_cards, bid, distinct_cards_non_joker=None, hand_with_joker_replaced=None):
        self.hand = hand
        self.distinct_cards = distinct_cards
        self.bid = bid
        self.distinct_cards_non_joker = distinct_cards_non_joker
        self.hand_with_joker_replaced = hand_with_joker_replaced

def get_distinct(hand):
    return(list(set([l for l in hand])))

def count_x_in_hand(x, hand):
    return len([y for y in hand if y == x])

def part1():
    lines = utils.get_lines("inputs/07.txt")
    sort_order = "".join(['A', 'K', 'Q', 'J', 'T', '9', '8', '7', '6', '5', '4', '3', '2'])
    sort_order = sort_order[::-1]

    hand_infos = [HandInfo(line.split()[0], get_distinct(line.split()[0]), int(line.split()[1])) for line in lines]

    # Determine which hands correspond to which hand types.
    five_of_a_kind = [x for x in hand_infos if len(x.distinct_cards) == 1]
    four_of_a_kind = [x for x in hand_infos if len(x.distinct_cards) == 2 and any([count_x_in_hand(card, x.hand) == 4 for card in x.distinct_cards])]
    full_house = [x for x in hand_infos if len(x.distinct_cards) == 2 and any([count_x_in_hand(card, x.hand) == 3 for card in x.distinct_cards])]
    three_of_a_kind = [x for x in hand_infos if len(x.distinct_cards) == 3 and any([count_x_in_hand(card, x.hand) == 3 for card in x.distinct_cards])]
    one_pair = [x for x in hand_infos if len(x.distinct_cards) == 4]
    high_card = [x for x in hand_infos if len(x.distinct_cards) == 5]
    two_pair = [x for x in hand_infos if x not in five_of_a_kind and x not in four_of_a_kind and x not in full_house and x not in three_of_a_kind and x not in one_pair and x not in high_card]

    # Sort hands within each group
    current_rank = 1
    total = 0
    for group in [high_card, one_pair, two_pair, three_of_a_kind, full_house, four_of_a_kind, five_of_a_kind]:
        sorted_group = sorted(group, key = lambda x : [sort_order.index(c) for c in x.hand])
        for hand_info in sorted_group:
            total += current_rank * hand_info.bid
            current_rank += 1

    print(total)
    # Time: 09:19:35
    # Rank: 34187

def part2():    
    lines = utils.get_lines("inputs/07.txt")
    sort_order = "".join(['A', 'K', 'Q', 'T', '9', '8', '7', '6', '5', '4', '3', '2', 'J'])
    sort_order = sort_order[::-1]

    hand_infos = [HandInfo(line.split()[0], get_distinct(line.split()[0]), int(line.split()[1])) for line in lines]
    for index in range(len(hand_infos)):
        if "J" not in hand_infos[index].hand:
            hand_infos[index].hand_with_joker_replaced = hand_infos[index].hand
        else:
            # Idea: replace joker with non-joker card having highest count
            hand_no_joker = hand_infos[index].hand.replace("J", "")
            if hand_no_joker == '':
                joker_transitions_to = 'J'
            else:
                joker_transitions_to = max(hand_no_joker, key=lambda x: hand_no_joker.count(x))

            hand_infos[index].hand_with_joker_replaced = hand_infos[index].hand.replace("J", joker_transitions_to)

        hand_infos[index].distinct_cards_non_joker = get_distinct(hand_infos[index].hand_with_joker_replaced)
    
    # Determine which hands correspond to which hand types
    five_of_a_kind = [x for x in hand_infos if len(x.distinct_cards_non_joker) == 1]
    four_of_a_kind = [x for x in hand_infos if len(x.distinct_cards_non_joker) == 2 and any([count_x_in_hand(card, x.hand_with_joker_replaced) == 4 for card in x.distinct_cards_non_joker])]
    full_house = [x for x in hand_infos if len(x.distinct_cards_non_joker) == 2 and any([count_x_in_hand(card, x.hand_with_joker_replaced) == 3 for card in x.distinct_cards_non_joker])]
    three_of_a_kind = [x for x in hand_infos if len(x.distinct_cards_non_joker) == 3 and any([count_x_in_hand(card, x.hand_with_joker_replaced) == 3 for card in x.distinct_cards_non_joker])]
    one_pair = [x for x in hand_infos if len(x.distinct_cards_non_joker) == 4]
    high_card = [x for x in hand_infos if len(x.distinct_cards_non_joker) == 5]
    two_pair = [x for x in hand_infos if x not in five_of_a_kind and x not in four_of_a_kind and x not in full_house and x not in three_of_a_kind and x not in one_pair and x not in high_card]

    # Sort hands within each group
    current_rank = 1
    total = 0
    for group in [high_card, one_pair, two_pair, three_of_a_kind, full_house, four_of_a_kind, five_of_a_kind]:
        sorted_group = sorted(group, key = lambda x : [sort_order.index(c) for c in x.hand])
        for hand_info in sorted_group:
            total += current_rank * hand_info.bid
            current_rank += 1

    print(total)
    # Time: 22:24:57
    # Rank: 48763

if __name__ == '__main__':
    part1()
    part2()