'''https://adventofcode.com/2023/day/4'''

import utils

def part1():
    lines = utils.get_lines("inputs/04.txt")

    total = 0
    for line in lines:
        # Figure out the number of winning numbers you have via set intersection.
        cleaned = line.split(": ")[1].split(" | ")
        winning_nums = [int(x) for x in cleaned[0].split(" ") if x != '']
        your_nums = [int(x) for x in cleaned[1].split(" ") if x != '']
        intersecting_count = len(set(winning_nums) & set(your_nums))

        if intersecting_count > 0:
            total += 2 ** (intersecting_count - 1)

    print(total)
    # Time: 07:02:00
    # Rank: 46582

def part2():
    lines = utils.get_lines("inputs/04.txt")

    card_count = [1] * len(lines)
    for i in range(len(lines)):
        # Figure out the number of winning numbers you have via set intersection.
        cleaned = lines[i].split(": ")[1].split(" | ")
        winning_nums = [int(x) for x in cleaned[0].split(" ") if x != '']
        your_nums = [int(x) for x in cleaned[1].split(" ") if x != '']
        intersecting_count = len(set(winning_nums) & set(your_nums))

        for j in range(intersecting_count):
            # Add to your number of scratch cards
            card_count[i + j + 1] += card_count[i]

    print(sum(card_count))
    # Time: 07:09:19 
    # Rank: 36262

if __name__ == '__main__':
    part1()
    part2()