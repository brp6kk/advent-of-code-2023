'''https://adventofcode.com/2023/day/15'''

import utils
from collections import OrderedDict

def hash(string):
    '''Holiday ASCII String Helper algorithm'''
    total = 0
    for char in string:
        total += ord(char)
        total *= 17
        total %= 256
    return total

def part1():
    line = utils.get_lines("inputs/15.txt")
    sequence = line[0].split(",")
    total = sum([hash(item) for item in sequence])
    print(total)
    # Time: 06:54:14
    # Rank: 20964

def part2():
    line = utils.get_lines("inputs/15.txt")
    sequence = line[0].split(",")
    boxes = dict()

    for item in sequence:
        if item[-1] == '-':
            # Remove label from the appropriate box
            label = item[:-1]
            box_num = hash(label)
            if box_num in boxes.keys() and label in boxes[box_num].keys():
                del boxes[box_num][label]
        else:
            # If label does not exist, then add; otherwise, replace existing label
            label, focal_length = item.split("=")
            box_num = hash(label)
            if box_num not in boxes.keys():
                boxes[box_num] = OrderedDict()
            boxes[box_num][label] = int(focal_length)
                
    # Get total focusing power, as defined by problem
    total = 0
    for box_num in boxes.keys():
        lenses = list(boxes[box_num].values())
        for i in range(len(boxes[box_num].keys())):
            total += (box_num + 1) * (i + 1) * (lenses[i])

    print(total)
    # Time: 09:16:38
    # Rank: 19564

if __name__ == '__main__':
    part1()
    part2()