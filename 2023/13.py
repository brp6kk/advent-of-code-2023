'''https://adventofcode.com/2023/day/13'''

import utils
import numpy as np

def num_differences_equal_to(grid1, grid2, num_differences):
    '''
    Assumes grid1 & grid2 are same dimensions; 
    returns True if there are num_differences different elements between grid1 & grid2
    '''
    count = 0
    for y in range(len(grid1)):
        for x in range(len(grid1[y])):
            if grid1[y][x] != grid2[y][x] and count == num_differences:
                # Return early since we know the number of differences exceeds the parameter
                return False
            elif grid1[y][x] != grid2[y][x]:
                count += 1

    return count == num_differences

def check_for_horizontal_reflection(pattern, num_differences):
    '''
    Return a positive integer indicating the index where the reflecting line is between index - 1 and index,
    or -1 if no reflecting line exists.
    pattern: 2D list representing the elements of an input pattern
    num_differences: the number of differences allowed for this to be considered a reflection
    '''
    # Each iteration considers a reflecting line, moving down the pattern each step
    for i in range(1, len(pattern)):
        # Be sure not to check past the edge of the pattern
        num_lines_to_check = min(i, len(pattern) - i)

        above_lines = []
        for j in range(i - 1, i - num_lines_to_check - 1, -1):
            above_lines.append(pattern[j])

        below_lines = []
        for j in range(i, i + num_lines_to_check):
            below_lines.append(pattern[j])

        if num_differences_equal_to(above_lines, below_lines, num_differences):
            return i
        
    return -1

def get_reflection_summary(num_differences_in_reflection):
    '''Calculate output as defined by problem statement.'''
    lines = utils.get_lines("inputs/13.txt")
    patterns = [[]]
    for line in lines:
        clean_line = list(line.strip())
        if clean_line != []:
            patterns[-1].append(clean_line)
        else:
            patterns.append([])
    
    # Assumption: there is exactly one point of reflection
    total = 0
    for pattern in patterns:
        # Check for horizontal reflection
        axis = check_for_horizontal_reflection(pattern, num_differences_in_reflection)
        if axis != -1:
            total += 100 * axis
            continue
            
        # Check for vertical reflection
        flipped_pattern = np.swapaxes(pattern, 0, 1)
        axis = check_for_horizontal_reflection(flipped_pattern, num_differences_in_reflection)
        total += axis
    
    return total

def part1():
    print(get_reflection_summary(0))
    # Time: 07:47:10
    # Rank: 16344

def part2():
    print(get_reflection_summary(1))
    # Time: 09:25:21  
    # Rank: 14287

if __name__ == '__main__':
    part1()
    part2()