'''https://adventofcode.com/2023/day/20'''

import utils
from enum import Enum
import math 

class Type(Enum):
    BROADCASTER = 'B'
    FLIPFLOP = '%'
    CONJUNCTION = '&'

class Pulse(Enum):
    LOW = 0
    HIGH = 1

class Module():
    def __init__(self, name, type, destinations):
        self.name = name
        self.type = type
        self.destinations = destinations
    
    def process_input(self, input_module, input_pulse):
        raise NotImplementedError()
    
    def __str__(self):
        return f"Name: {self.name}; Type: {self.type.value}; Destinations: {str(self.destinations)}"

class FlipFlop(Module):
    def __init__(self, name, destinations):
        super().__init__(name, Type.FLIPFLOP, destinations)
        self.output_pulse = Pulse.LOW
    
    def process_input(self, input_module, input_pulse):
        if input_pulse == Pulse.HIGH:
            return None
        
        self.output_pulse = Pulse(1 - self.output_pulse.value)
        return self.output_pulse

    def __str__(self):
        str_start = super().__str__()
        return str_start + f"; Outputs: {self.output_pulse}"

class Conjunction(Module):
    def __init__(self, name, destinations):
        super().__init__(name, Type.CONJUNCTION, destinations)
        self.inputs = dict()

    def process_input(self, input_module, input_pulse):
        self.inputs[input_module.name] = input_pulse

        if all([x == Pulse.HIGH for x in self.inputs.values()]):
            return Pulse.LOW
        
        return Pulse.HIGH
    
    def add_input(self, input_module):
        self.inputs[input_module.name] = Pulse.LOW

    def __str__(self):
        str_start = super().__str__()
        return str_start + f"; Inputs: {str(self.inputs)}"

class Broadcaster(Module):
    def __init__(self, name, destinations):
        super().__init__(name, Type.BROADCASTER, destinations)
    
    def process_input(self, input_module, input_pulse):
        return input_pulse

def create_modules_from_lines(lines):
    '''Create modules, using name as a key for easy searchability'''
    modules = dict()
    for line in lines:
        source, destination_str = line.strip().split(" -> ")
        destinations = destination_str.split(", ")

        if source[0] == Type.FLIPFLOP.value:
            source = source[1:]
            modules[source] = FlipFlop(source, destinations)
        elif source[0] == Type.CONJUNCTION.value:
            source = source[1:]
            modules[source] = Conjunction(source, destinations)
        elif source == 'broadcaster':
            modules[source] = Broadcaster(source, destinations)
        else:
            raise Exception("Invalid source")
        
    # Find all inputs for each conjunction module
    for m in modules.values():
        for d in m.destinations:
            if d in modules.keys() and modules[d].type == Type.CONJUNCTION:
                modules[d].add_input(m)

    return modules

def part1():
    lines = utils.get_lines("inputs/20.txt")

    modules = create_modules_from_lines(lines)
    
    counts = {
        Pulse.LOW: 0,
        Pulse.HIGH: 0
    }
    for _ in range(1000):
        # Move through architecture
        stack = [("broadcaster", Pulse.LOW, "broadcaster")]
        while len(stack) > 0:
            source, pulse, destination = stack.pop(0)
            if pulse is None:
                continue
            counts[pulse] += 1
            if destination not in modules.keys():
                continue
            next_pulse = modules[destination].process_input(modules[source], pulse)
            stack.extend([(destination, next_pulse, farther_destination) for farther_destination in modules[destination].destinations])

    print(counts[Pulse.LOW] * counts[Pulse.HIGH])
    # Time: 23:11:30
    # Rank: 15443

def part2():
    lines = utils.get_lines("inputs/20.txt")

    modules = create_modules_from_lines(lines)
        
    # For vd (which is the input for rx, the output), save inputs
    vd_inputs = dict()
    for m in modules.values():
        for d in m.destinations:
            if d == 'vd':
                vd_inputs[m.name] = 0

    for i in range(1,1000000):
        # Move through architecture
        stack = [("broadcaster", Pulse.LOW, "broadcaster")]
        while len(stack) > 0:
            source, pulse, destination = stack.pop(0)
            if pulse is None or destination not in modules.keys():
                continue
            if destination == 'vd' and pulse == Pulse.HIGH:
                vd_inputs[source] = i
            next_pulse = modules[destination].process_input(modules[source], pulse)
            stack.extend([(destination, next_pulse, farther_destination) for farther_destination in modules[destination].destinations])
            
        if all(list(vd_inputs.values())):
            # The only input to rx is vd, a conjunction
            # vd only emits a low pulse when all its inputs emit high pulses
            # Apparently, the emission of high pulses is cyclical
            print(math.lcm(*list(vd_inputs.values())))
            # Time: >24h
            # Rank: 13201
            return

if __name__ == '__main__':
    part1()
    part2()