'''https://adventofcode.com/2023/day/19'''

import utils
import math
import copy

class Rule():
    def __init__(self, category, operator, num_to_check, destination):
        self.category = category # x, m, a, or s
        self.operator = operator # > or <
        self.num_to_check = num_to_check
        self.destination = destination # String corresponding to label, or A or R

def part1():
    lines = utils.get_lines("inputs/19.txt")
    workflows = dict()

    index = 0
    while len(lines[index].strip()) != 0:
        name, unparsed_flow = lines[index].strip().split("{")
        unparsed_flow = unparsed_flow[:-1]
        workflows[name] = []

        for unparsed_step in unparsed_flow.split(","):
            split_rule = unparsed_step.split(":")
            if len(split_rule) == 1: # Last element, always should be true
                workflows[name].append(Rule("a", ">", 0, split_rule[0]))
                continue

            category = split_rule[0][0]
            operator = split_rule[0][1]
            destination = split_rule[1]
            num_to_check = int(split_rule[0][2:])
            workflows[name].append(Rule(category, operator, num_to_check, destination))
        index += 1

    index += 1

    accepted = []
    for line in lines[index:]:
        raw_data = line.strip()[1:-1].split(",")
        part = dict()
        for d in raw_data:
            key, value = d.split("=")
            part[key] = int(value)
        found = False 

        category = "in"
        while not found:
            rules = workflows[category]
            for rule in rules:
                if (rule.operator == "<" and part[rule.category] < rule.num_to_check) or (rule.operator == ">" and part[rule.category] > rule.num_to_check):
                    category = rule.destination
                    break

            if category == 'A':
                accepted.append(part)
                found = True
            elif category == 'R':
                found = True

    total = 0
    for item in accepted:
        total += item["x"] + item["m"] + item["a"] + item["s"]

    print(total)
    # Time: 13:10:32
    # Rank: 17370

def part2():
    lines = utils.get_lines("inputs/19.txt")
    workflows = dict()

    index = 0
    while len(lines[index].strip()) != 0:
        name, unparsed_flow = lines[index].strip().split("{")
        unparsed_flow = unparsed_flow[:-1]
        workflows[name] = []

        for unparsed_step in unparsed_flow.split(","):
            split_rule = unparsed_step.split(":")
            if len(split_rule) == 1: # Last element, always should be true
                workflows[name].append(Rule("a", ">", 0, split_rule[0]))
                continue

            category = split_rule[0][0]
            operator = split_rule[0][1]
            destination = split_rule[1]
            num_to_check = int(split_rule[0][2:])
            workflows[name].append(Rule(category, operator, num_to_check, destination))
        index += 1

    stack = [("in", [list(range(1, 4001)) for _ in range(4)])]
    category_labels = ["x", "m", "a", "s"]
    total = 0
    while len(stack) > 0:
        category, xmas_count = stack.pop(0)
        xmas_count_invalid = copy.deepcopy(xmas_count)

        for rule in workflows[category]:
            xmas_count_valid = copy.deepcopy(xmas_count_invalid)
            xmas_count_invalid = copy.deepcopy(xmas_count_invalid) # Count of valid paths when this rule is evaluated as false
            index_to_modify = category_labels.index(rule.category)

            # Remove items that cannot be valid if this rule is evaluated as true.
            to_remove_from_valid = list(range(1, rule.num_to_check + 1)) if rule.operator == ">" else list(range(rule.num_to_check, 4001))
            for item in to_remove_from_valid:
                if item in xmas_count_valid[index_to_modify]:
                    xmas_count_valid[index_to_modify].remove(item)
        
            if rule.destination == "A":
                # Reached the end of a path that leads to acceptance, so count how many possible ways we can get here.
                total += math.prod([len(valid_category_values) for valid_category_values in xmas_count_valid])
            elif rule.destination != "R":
                # Rejected items are not considered. All other items must be further evaluated.
                stack.append((rule.destination, xmas_count_valid))

            # Remove items that are valid if this rule is evaluated as true.
            # The next rule in this workflow is considered on items that do not pass the current rule.
            to_remove_from_invalid = [item for item in range(1, 4001) if item not in to_remove_from_valid]
            for item in to_remove_from_invalid:
                if item in xmas_count_invalid[index_to_modify]:
                    xmas_count_invalid[index_to_modify].remove(item)
    
    print(total)
    # Time: 17:30:01
    # Rank: 12954

if __name__ == '__main__':
    part1()
    part2()