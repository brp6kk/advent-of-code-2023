'''https://adventofcode.com/2023/day/10'''

import utils

class Coord:
    def __init__(self, y, x, is_pipe=False, is_underlying_pipe=False, is_visited=False):
        self.y = y
        self.x = x
        self.is_pipe = is_pipe
        self.is_underlying_pipe = is_underlying_pipe
        self.is_visited = is_visited

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __hash__(self):
        return hash(tuple(sorted(self.__dict__.items())))

def find_starting_coord(lines):
    starting_coord = Coord(-1, -1)
    for y in range(len(lines)):
        for x in range(len(lines[y])):
            if lines[y][x] == 'S':
                starting_coord.x = x
                starting_coord.y = y
                return starting_coord

def is_invalid_location(lines, y, x):
    return y < 0 or y >= len(lines) or x < 0 or x >= len(lines[y])

# If approaching from key, move in direction of value
traversal_movements = {
    "|": {
        Coord(1, 0): Coord(1, 0),
        Coord(-1, 0): Coord(-1, 0)
    },
    "-": {
        Coord(0, 1): Coord(0, 1),
        Coord(0, -1): Coord(0, -1)
    },
    "L": {
        Coord(1, 0): Coord(0, 1),
        Coord(0, -1): Coord(-1, 0)
    },
    "J": {
        Coord(1, 0): Coord(0, -1),
        Coord(0, 1): Coord(-1, 0)
    },
    "7": {
        Coord(-1, 0): Coord(0, -1),
        Coord(0, 1): Coord(1, 0)
    },
    "F": {
        Coord(-1, 0): Coord(0, 1),
        Coord(0, -1): Coord(1, 0)
    }
}

def part1():  
    lines = utils.get_lines("inputs/10.txt")

    loop_starting_coord = find_starting_coord(lines)
    movement = Coord(-1, 0) # Hard code starting movement
    current_coord = Coord(loop_starting_coord.y + movement.y, loop_starting_coord.x + movement.x)
    movement_counter = 1
    while current_coord != loop_starting_coord:
        # Traverse entire loop
        pipe_type = lines[current_coord.y][current_coord.x]
        movement = traversal_movements[pipe_type][movement]
        current_coord = Coord(current_coord.y + movement.y, current_coord.x + movement.x)
        movement_counter += 1

    print(movement_counter / 2)
    # Time: 07:25:29
    # Rank: 21060

def part2():
    lines = utils.get_lines("inputs/10.txt")
    pipe_shapes = {
        "|": [[False, True, False]] * 3,
        "-": [[False] * 3, [True] * 3, [False] * 3],
        "L": [[False, True, False], [False, True, True], [False] * 3], 
        "J": [[False, True, False], [True, True, False], [False] * 3],
        "7": [[False] * 3, [True, True, False], [False, True, False]],
        "F": [[False] * 3, [False, True, True], [False, True, False]],
        "S": [[False, True, False], [True, True, False], [False] * 3]
    }

    # Get locations of all pipes of loop
    loop_starting_coord = find_starting_coord(lines)
    all_loop_coords = [loop_starting_coord]
    movement = Coord(-1, 0) # Hard code starting movement
    current_coord = Coord(loop_starting_coord.y + movement.y, loop_starting_coord.x + movement.x)
    while current_coord != loop_starting_coord:
        all_loop_coords.append(current_coord)
        pipe_type = lines[current_coord.y][current_coord.x]
        movement = traversal_movements[pipe_type][movement]
        current_coord = Coord(current_coord.y + movement.y, current_coord.x + movement.x)

    # Expand map so that one location -> 3x3 grid of Coords,
    # using is_pipe to build the actual pipe shapes, 
    # is_underlying_pipe to indicate which Coords were created from a single pipe location on original map
    expanded_map = []
    for y in range(len(lines)):
        expanded_map.extend([[], [], []])
        for x in range(len(lines[y].rstrip())):
            if Coord(y, x) not in all_loop_coords:
                is_underlying_pipe = False
                is_pipes = [[False] * 3] * 3
            else:
                is_underlying_pipe = True
                is_pipes = pipe_shapes[lines[y][x]]

            for ny in range(3):
                for nx in range(3):
                    expanded_map[y * 3 + ny].append(Coord(y * 3 + ny, x * 3 + nx, is_pipes[ny][nx], is_underlying_pipe))

    # Flood fill on expanded map
    stack = [(0, 0)]
    while len(stack) > 0:
        y, x = stack.pop()
        if is_invalid_location(expanded_map, y, x) or expanded_map[y][x].is_pipe or expanded_map[y][x].is_visited:
            continue

        expanded_map[y][x].is_visited = True
        stack.append((y - 1, x))
        stack.append((y + 1, x))
        stack.append((y, x - 1))
        stack.append((y, x + 1))
    
    # Count locations not visited and not originally pipe
    not_visited_counter = 0
    for y in range(len(expanded_map)):
        for x in range(len(expanded_map[y])):
            if not (expanded_map[y][x].is_visited or expanded_map[y][x].is_underlying_pipe):
                not_visited_counter += 1
    
    # Since we expanded the map, need to reduce the counter to get true count 
    print(not_visited_counter / 9)
    # Time: 13:15:28
    # Rank: 17828

if __name__ == '__main__':
    part1()
    part2()