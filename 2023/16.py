'''https://adventofcode.com/2023/day/16'''

import utils
from enum import Enum

class Direction(Enum):
    LEFT = 1
    RIGHT = 2
    UP = 3
    DOWN = 4

def traverse_contraption(contraption, starting_location, starting_direction, visited_map):
    stack = [(starting_location, starting_direction)]
    is_start = True

    while len(stack) > 0:
        current_location, current_direction = stack.pop()

        if not is_start:
            # Starting location is always out of bounds
            if current_direction in visited_map[current_location[0]][current_location[1]]:
                continue
            visited_map[current_location[0]][current_location[1]].append(current_direction)
        else:
            is_start = False

        match current_direction:
            # Determine next location based on the direction currently traveling in.
            # Continue if next location is out of bounds
            case Direction.LEFT:
                if current_location[1] == 0:
                    continue
                next_location = (current_location[0], current_location[1] - 1)
            case Direction.RIGHT:
                if current_location[1] == len(contraption[0]) - 1:
                    continue
                next_location = (current_location[0], current_location[1] + 1)
            case Direction.UP:
                if current_location[0] == 0:
                    continue
                next_location = (current_location[0] - 1, current_location[1])
            case Direction.DOWN:
                if current_location[0] == len(contraption) - 1:
                    continue
                next_location = (current_location[0] + 1, current_location[1])
        
        next_grid_contents = contraption[next_location[0]][next_location[1]]

        # After moving to next location, direction of travel potentially changes
        if next_grid_contents == '|' and (current_direction == Direction.LEFT or current_direction == Direction.RIGHT):
            stack.append((next_location, Direction.UP))
            stack.append((next_location, Direction.DOWN))
        elif next_grid_contents == '-' and (current_direction == Direction.UP or current_direction == Direction.DOWN):
            stack.append((next_location, Direction.LEFT))
            stack.append((next_location, Direction.RIGHT))
        elif next_grid_contents == '/':
            match current_direction:
                case Direction.LEFT:
                    stack.append((next_location, Direction.DOWN))
                case Direction.RIGHT: 
                    stack.append((next_location, Direction.UP))
                case Direction.UP:
                    stack.append((next_location, Direction.RIGHT))
                case Direction.DOWN:
                    stack.append((next_location, Direction.LEFT))
        elif next_grid_contents == '\\':
            match current_direction:
                case Direction.LEFT:
                    stack.append((next_location, Direction.UP))
                case Direction.RIGHT: 
                    stack.append((next_location, Direction.DOWN))
                case Direction.UP:
                    stack.append((next_location, Direction.LEFT))
                case Direction.DOWN:
                    stack.append((next_location, Direction.RIGHT))
        else:
            stack.append((next_location, current_direction))

def calculate_energized_count(grid):
    '''Count the number of elements in grid having at been visited at least once.'''
    energized_count = 0
    for y in range(len(grid)):
        for x in range(len(grid[y])):
            if len(grid[y][x]) > 0:
                energized_count += 1
    return energized_count

def part1():
    lines = utils.get_lines("inputs/16.txt") 
    lines = [x.rstrip() for x in lines]
    grid = [[[] for _ in range(len(lines[0]))] for _ in range(len(lines))]
    starting_location = (0, -1)
    starting_direction = Direction.RIGHT

    traverse_contraption(lines, starting_location, starting_direction, grid)

    print(calculate_energized_count(grid))
    # Time: 10:12:44
    # Rank: 16991

def part2():
    lines = utils.get_lines("inputs/16.txt") 
    lines = [x.rstrip() for x in lines]
    
    # Want to test along all edges
    test_cases = []
    test_cases = test_cases + [((y, -1), Direction.RIGHT) for y in range(len(lines))]
    test_cases = test_cases + [((y, len(lines)), Direction.LEFT) for y in range(len(lines))]
    test_cases = test_cases + [((-1, x), Direction.DOWN) for x in range(len(lines[0]))]
    test_cases = test_cases + [((len(lines[0]), x), Direction.UP) for x in range(len(lines[0]))]

    max_energized_count = 0
    for (starting_location, starting_direction) in test_cases:
        grid = [[[] for _ in range(len(lines[0]))] for _ in range(len(lines))]
        traverse_contraption(lines, starting_location, starting_direction, grid)
        max_energized_count = max(max_energized_count, calculate_energized_count(grid))

    print(max_energized_count)
    # Time: 10:28:14
    # Rank: 15983

if __name__ == '__main__':
    part1()
    part2()