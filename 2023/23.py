'''https://adventofcode.com/2023/day/23'''

import utils

class Point():
    def __init__(self, y, x):
        self.y = y 
        self.x = x

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y
    
    def __hash__(self):
        return hash((self.x, self.y))

def is_on_map(point, map):
    return point.y >= 0 and point.y < len(map) and point.x >= 0 and point.x < len(map[0])

def can_travel_down_slope(slope, direction):
    valid = [
        ("^", (-1, 0)),
        ("v", (1, 0)),
        ("<", (0, -1)),
        (">", (0, 1))
    ]
    return (slope, direction) in valid

def part1():
    lines = utils.get_lines("inputs/23.txt")
    start = Point(0, lines[0].find("."))
    end = Point(len(lines) - 1, lines[-1].find("."))

    visited = set()
    stack = [(0, visited, start)]
    max_count = 0
   
    while len(stack) > 0:
        current_count, current_visited, current_point = stack.pop()
        
        if current_point == end and current_count > max_count:
            max_count = current_count
            continue

        current_count += 1
        current_visited = current_visited.union([current_point])
        for (dy, dx) in [(1, 0), (-1, 0), (0, 1), (0, -1)]:
            new_point = Point(current_point.y + dy, current_point.x + dx)
            if is_on_map(new_point, lines) and new_point not in current_visited and (lines[new_point.y][new_point.x] == '.' or can_travel_down_slope(lines[new_point.y][new_point.x], (dy, dx))):
                stack.append((current_count, current_visited, new_point))
    
    print(max_count)
    # Time: 09:46:13
    # Rank: 8801

def part2():
    pass

if __name__ == '__main__':
    part1()
    part2()