'''https://adventofcode.com/2023/day/8'''

import utils
import math

class Node():
    def __init__(this, current, left, right):
        this.current = current
        this.left = left
        this.right = right

def part1():
    lines = utils.get_lines("inputs/08.txt")
    directions = lines[0].rstrip()
    network = dict()

    # Create dict to easily get left/right node given some input node
    for i in range(2, len(lines)):
        current_node, lr = lines[i].split(" = ")
        left = lr[1:4]
        right = lr[6:9]
        network[current_node] = Node(current_node, left, right)

    current_node = "AAA"
    step_count = 0
    # Navigate network until reaching end
    while current_node != "ZZZ":
        this_step = directions[step_count % len(directions)]
        if this_step == 'L':
            current_node = network[current_node].left
        else:
            current_node = network[current_node].right
        step_count += 1
    
    print(step_count)
    # Time: 07:04:03
    # Rank: 34281

def part2():
    lines = utils.get_lines("inputs/08.txt")
    directions = lines[0].rstrip()
    network = dict()

    # Create dict to easily get left/right node given some input node
    for i in range(2, len(lines)):
        current_node, lr = lines[i].split(" = ")
        left = lr[1:4]
        right = lr[6:9]
        network[current_node] = Node(current_node, left, right)

    current_nodes = [x for x in network.keys() if x[2] == 'A']
    step_count = [0] * len(current_nodes)
    # For all nodes that end in 'A', find length of loop
    for i in range(len(current_nodes)):
        current_node = current_nodes[i]
        while current_node[2] != 'Z':
            thisstep = directions[step_count[i] % len(directions)]
            if thisstep == 'L':
                current_node = network[current_node].left
            else:
                current_node = network[current_node].right
            step_count[i] += 1
    
    # LCM = first step at which we'll be at end of loop for all loops
    final_lcm = step_count[0]
    for i in range(1, len(step_count)):
        final_lcm = math.lcm(final_lcm, step_count[i])
        
    print(final_lcm)
    # Time: 07:40:06
    # Rank: 23160

if __name__ == '__main__':
    part1()
    part2()