'''https://adventofcode.com/2023/day/1'''

import utils
import math

def part1():
    lines = utils.get_lines('inputs/01.txt')
    total = 0
    
    for line in lines:
        str_num = ""

        # Get first digit in line
        for char in line:
            if char.isdigit():
                str_num += char
                break

        # Get last digit in line
        for char in line[::-1]:
            if char.isdigit():
                str_num += char
                break
        
        # Combine both digits into one integer, then add to running sum
        int_num = int(str_num)
        total += int_num
    
    print(total)
    # Time: 16:02:19
    # Rank: 121596

def part2():
    lines = utils.get_lines('inputs/01.txt')
    possible_num_words = ["zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]
    total = 0

    for line in lines:
        str_num = ""
        possible_num_word_first_occurrences = []
        possible_num_word_last_occurrences = []
        for i in range(10):
            # Find index for the first/last occurrence of each possible number word
            try:
                possible_num_word_first_occurrences.append(line.index(possible_num_words[i]))
            except ValueError:
                possible_num_word_first_occurrences.append(math.inf)
            try:
                possible_num_word_last_occurrences.append(line.rindex(possible_num_words[i]))
            except ValueError:
                possible_num_word_last_occurrences.append(-1)

        first = ""
        for i in range(len(line)):
            if line[i].isdigit() and all([i < num for num in possible_num_word_first_occurrences]):
                # Digit is the first occurrence of a number in the string
                first = line[i]
                break
        if first == '':
            # Number word is the first occurrence of a number in the string
            first = str(possible_num_word_first_occurrences.index(min(possible_num_word_first_occurrences)))
        
        last = ""
        for i in range(len(line) - 1, -1, -1):
            if line[i].isdigit() and all([i > num for num in possible_num_word_last_occurrences]):
                # Digit is the last occurrence of a number in the string
                last = line[i]
                break
        if last == '':
            # Number word is the last occurrence of a number in the string
            last = str(possible_num_word_last_occurrences.index(max(possible_num_word_last_occurrences)))

        str_num = first + last
        int_num = int(str_num)
        total += int_num

    print(total)
    # Time: 16:36:53
    # Rank: 84545

if __name__ == '__main__':
    part1()
    part2()