'''https://adventofcode.com/2023/day/21'''

import utils

def is_valid_location(grid, y, x):
    return y >= 0 and y < len(grid) and x >= 0 and x < len(grid[0]) and grid[y][x] != '#'

def get_valid_next_steps(grid, y, x):
    valid_next_steps = []
    movements = [(1, 0), (-1, 0), (0, 1), (0, -1)]
    for (dy, dx) in movements:
        if is_valid_location(grid, y + dy, x + dx):
            valid_next_steps.append((y + dy, x + dx))

    return valid_next_steps

def part1():
    lines = utils.get_lines("inputs/21.txt")
    clean_lines = []
    starting_position = None

    # Clean input & find starting position
    for y in range(len(lines)):
        clean_lines.append(lines[y].strip())
        if starting_position is None:
            for x in range(len(clean_lines[y])):
                if clean_lines[y][x] == 'S':
                    starting_position = (y, x)
                    break

    # Calculate set of all possible positions based on previous possible positions
    possible_positions = set([starting_position])
    for _ in range(64):
        new_possible_positions = set()
        for position in possible_positions:
            new_possible_positions.update(get_valid_next_steps(clean_lines, position[0], position[1]))
        possible_positions = new_possible_positions

    print(len(possible_positions))
    # Time: 07:19:49
    # Rank: 11934

def part2():
    pass

if __name__ == '__main__':
    part1()
    part2()