'''https://adventofcode.com/2023/day/24'''

import utils
import numpy as np
import z3

class Point():
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

class Hailstone():
    def __init__(self, px, py, pz, vx, vy, vz):
        self.initial_position = Point(px, py, pz)
        self.velocity = Point(vx, vy, vz)

    def calculate_intersection_time(self, other):
        # Matrices found by writing out parametric equations & settings xs, ys equal
        a = np.array([[self.velocity.x, -other.velocity.x], [self.velocity.y, -other.velocity.y]])
        b = np.array([other.initial_position.x - self.initial_position.x, other.initial_position.y - self.initial_position.y])
        
        try:
            return np.linalg.solve(a, b)
        except np.linalg.LinAlgError: # Parallel
            return None
        
    def calculate_intersection_xy_point(self, other):
        t = self.calculate_intersection_time(other)
        if t is None or t[0] < 0 or t[1] < 0:
            return None
        
        x = self.initial_position.x + t[0] * self.velocity.x
        y = self.initial_position.y + t[0] * self.velocity.y
        return Point(x, y, None)

def get_hailstones():
    lines = utils.get_lines("inputs/24.txt")
    hailstones = []
    for line in lines:
        position_str, velocity_str = line.strip().split(" @ ")
        px, py, pz = [int(val) for val in position_str.split(", ")]
        vx, vy, vz = [int(val) for val in velocity_str.split(", ")]
        hailstones.append(Hailstone(px, py, pz, vx, vy, vz))
    return hailstones

def part1():
    hailstones = get_hailstones()

    count = 0
    min = 200000000000000
    max = 400000000000000
    for i in range(len(hailstones)):
        for j in range(i + 1, len(hailstones)):
            result = hailstones[i].calculate_intersection_xy_point(hailstones[j])

            if result is None:
                continue
            if result.x >= min and result.x <= max and result.y >= min and result.y <= max:
                count += 1

    print(count)
    # Rank: 15:22:31
    # Time: 10120

def part2():
    # Only need 3 hailstones for the equation
    hailstones = get_hailstones()[:3]
    stone_p_x, stone_p_y, stone_p_z, stone_v_x, stone_v_y, stone_v_z, time_0, time_1, time_2 = z3.Reals("stone_p_x, stone_p_y, stone_p_z, stone_v_x, stone_v_y, stone_v_z, time_1, time_2, time_3")
    equations = [
        stone_p_x + time_0 * stone_v_x == hailstones[0].initial_position.x + time_0 * hailstones[0].velocity.x,
        stone_p_y + time_0 * stone_v_y == hailstones[0].initial_position.y + time_0 * hailstones[0].velocity.y,
        stone_p_z + time_0 * stone_v_z == hailstones[0].initial_position.z + time_0 * hailstones[0].velocity.z,
        stone_p_x + time_1 * stone_v_x == hailstones[1].initial_position.x + time_1 * hailstones[1].velocity.x,
        stone_p_y + time_1 * stone_v_y == hailstones[1].initial_position.y + time_1 * hailstones[1].velocity.y,
        stone_p_z + time_1 * stone_v_z == hailstones[1].initial_position.z + time_1 * hailstones[1].velocity.z,
        stone_p_x + time_2 * stone_v_x == hailstones[2].initial_position.x + time_2 * hailstones[2].velocity.x,
        stone_p_y + time_2 * stone_v_y == hailstones[2].initial_position.y + time_2 * hailstones[2].velocity.y,
        stone_p_z + time_2 * stone_v_z == hailstones[2].initial_position.z + time_2 * hailstones[2].velocity.z
    ]
    solver = z3.Solver()
    solver.add(*equations)
    solver.check()
    results = solver.model()
    starting_position = [results[stone_p_x].as_long(), results[stone_p_y].as_long(), results[stone_p_z].as_long()]
    print(sum(starting_position))
    # Time: >24h 
    # Rank: 7311

if __name__ == '__main__':
    part1()
    part2()