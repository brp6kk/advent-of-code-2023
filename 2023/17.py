'''https://adventofcode.com/2023/day/17'''

import utils
import heapq

def is_location_on_grid(y, x, grid):
    return y >= 0 and y < len(grid) and x >= 0 and x < len(grid[0])

def dijkstra(min_straight_length, max_straight_length):
    ''' 
    Run modified Dijkstra's algorithm.
    Edges exist between points on the grid with one of (y, x) same and the other off by one.
    Additionally, cannot turn if the path is not at least min_run length;
    cannot go straight if the path is more than max_run length.
    '''
    lines = utils.get_lines("inputs/17.txt")

    # Set up data structures
    # ((Location y, location x), (change in x to reach location, change in y to reach location), length of straight path)
    visited = set() 
    # grid[y][x] = heat at location (y, x)
    grid = [[int(lines[y][x]) for x in range(len(lines[y].strip()))] for y in range(len(lines))] 
    # (total heat of path to location, (location), (change in y, x), length of straight path)
    heap = [(grid[1][0], ((1, 0), (1, 0), 1)), (grid[0][1], ((0, 1), (0, 1), 1))] 
    heapq.heapify(heap)

    while len(heap) > 0:
        heat_total, ((y, x), (dy, dx), straight_path_length) = heapq.heappop(heap)

        if (((y, x), (dy, dx), straight_path_length) in visited):
            # Shorter path previously found
            continue
        elif (y == len(grid) - 1 and x == len(grid[0]) - 1) and straight_path_length >= min_straight_length:
            # Reached endpoint
            print(heat_total)
            return 

        visited.add(((y, x), (dy, dx), straight_path_length))
        
        if straight_path_length < min_straight_length:
            # Can only move straight because straight segment is too short
            possible_directions = [(dy, dx)]
        else:
            # Can't move backwards, but can move left/right
            possible_directions = [(0, 1), (0, -1), (1, 0), (-1, 0)]
            possible_directions.remove((-dy, -dx))
            if straight_path_length >= max_straight_length:
                # Straight segment too long
                possible_directions.remove((dy, dx))

        for dy_next, dx_next in possible_directions:
            y_next = y + dy_next
            x_next = x + dx_next
            if not is_location_on_grid(y_next, x_next, grid):
                continue

            # Add heat to total & push to heap
            heat_total_next = heat_total + grid[y_next][x_next]
            straight_path_length_next = straight_path_length + 1 if (dy_next, dx_next) == (dy, dx) else 1
            heapq.heappush(heap, (heat_total_next, ((y_next, x_next), (dy_next, dx_next), straight_path_length_next)))

def part1():
    dijkstra(1, 3)
    # Time: 19:31:03
    # Rank: 14492

def part2():
    dijkstra(4, 10)
    # Time: 20:49:03
    # Rank: 13655

if __name__ == '__main__':
    part1()
    part2()