'''https://adventofcode.com/2023/day/18'''

import utils
import math

def calculate_area(points, trench_length):
    '''Use shoelace formula to calculate area of trench
    points: List of corner points in trench, with point[0] duplicated at end
    trench_length: length in meters of trench
    '''
    sum1 = 0
    sum2 = 0
    for i in range(len(points) - 1):
        sum1 += points[i][0] * points[i + 1][1]
        sum2 += points[i + 1][0] * points[i][1]

    area = 0.5 * abs(sum1 - sum2) + trench_length // 2 + 1
    return area

def part1():
    lines = utils.get_lines("inputs/18.txt")
    movements = {
        "U": (-1, 0),
        "D": (1, 0),
        "R": (0, 1),
        "L": (0, -1)
    }

    point_list = [(0, 0)]
    trench_length = 0
    for line in lines:
        str_direction, str_distance, _ = line.split()
        direction = movements[str_direction]
        distance = int(str_distance)

        trench_length += distance 
        # Go distance in direction
        total_movement = tuple(map(math.prod, zip(direction, (distance, distance))))
        # Add movement to most recent point to get next point
        next_point = tuple(map(sum, zip(point_list[-1], total_movement)))
        point_list.append(next_point)

    print(calculate_area(point_list, trench_length))
    # Time: 10:38:24
    # Rank: 14496

def part2():
    lines = utils.get_lines("inputs/18.txt") 
    movements = [(0, 1), (1, 0), (0, -1), (-1, 0)]

    point_list = [(0, 0)]
    trench_length = 0
    for line in lines:
        _, _, hex = line.split()
        hex_distance = hex[2:7]
        hex_direction = int(hex[-2])
        distance = int(hex_distance, base=16)
        direction = movements[hex_direction]

        trench_length += distance
        # Go distance in direction
        total_movement = tuple(map(math.prod, zip(direction, (distance, distance))))
        # Add movement to most recent point to get next point
        next_point = tuple(map(sum, zip(point_list[-1], total_movement)))
        point_list.append(next_point)

    print(calculate_area(point_list, trench_length))
    # Time: 11:03:43
    # Rank: 9623

if __name__ == '__main__':
    part1()
    part2()