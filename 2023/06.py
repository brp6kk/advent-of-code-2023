'''https://adventofcode.com/2023/day/6'''

import utils

def part1():
    lines = utils.get_lines("inputs/06.txt")

    times = [int(x) for x in lines[0].split(": ")[1].split()]
    record_distances = [int(x) for x in lines[1].split(": ")[1].split()]

    total = 1
    for i in range(4):
        count = 0

        # Determine number of possible ways to beat the record
        for charge_time in range(times[i]):
            race_time = times[i] - charge_time
            distance = race_time * charge_time
            if distance > record_distances[i]:
                count += 1

        total *= count

    print(total)
    # Time: 07:07:42
    # Rank: 38085

def part2():
    lines = utils.get_lines("inputs/06.txt")

    time = int(''.join([x for x in lines[0].split(": ")[1] if x.isnumeric()]))
    record_distances = int(''.join([x for x in lines[1].split(": ")[1] if x.isnumeric()]))

    min_charge = time + 1
    max_charge = 0

    # Find lowest necessary charge to win
    for charge_time in range(time):
        race_time = time - charge_time
        distance = race_time * charge_time
        if distance > record_distances:
            min_charge = charge_time
            break

    # Find highest necessary charge to win
    for charge_time in range(time, -1, -1):
        race_time = time - charge_time 
        distance = race_time * charge_time
        if distance > record_distances:
            max_charge = charge_time
            break
    
    # Everything between min & max charge should also win.
    print(max_charge - min_charge + 1)
    # Time: 07:48:01
    # Rank: 38422

if __name__ == '__main__':
    part1()
    part2()