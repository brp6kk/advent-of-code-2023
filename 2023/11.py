'''https://adventofcode.com/2023/day/11'''

import utils
import numpy as np

def part1():
    lines = utils.get_lines("inputs/11.txt")  

    # Expand rows, starting from the bottom to avoid indexing issues
    for i in range(len(lines) - 1, -1, -1):
        lines[i] = list(lines[i].rstrip())
        if all([x == '.' for x in lines[i]]):
            lines.insert(i, lines[i])
    
    lines = list(np.swapaxes(lines, 0, 1))
    # Expand columns
    for i in range(len(lines) - 1, -1, -1):
        if all([x == '.' for x in lines[i]]):
            lines.insert(i, lines[i])

    lines = list(np.swapaxes(lines, 0, 1))

    # Store locations of galaxies
    galaxy_locations = []
    for y in range(len(lines)):
        for x in range(len(lines[y])):
            if lines[y][x] == '#':
                galaxy_locations.append((y, x))
    
    # Sum distances between all galaxies
    total = 0
    for galaxy_index1 in range(len(galaxy_locations)):
        for galaxy_index2 in range(galaxy_index1 + 1, len(galaxy_locations)):
            total += abs(galaxy_locations[galaxy_index1][0] - galaxy_locations[galaxy_index2][0]) + abs(galaxy_locations[galaxy_index1][1] - galaxy_locations[galaxy_index2][1])

    print(total)
    # Time: 07:18:47
    # Rank: 22116

def part2():
    lines = utils.get_lines("inputs/11.txt")  
    expansion_factor = 999999 # Add the original size (1) to get 1,000,000

    expanded_rows = []
    # Get indices of rows that should be expanded
    for i in range(len(lines)):
        lines[i] = list(lines[i].rstrip())
        if all([x == '.' for x in lines[i]]):
            expanded_rows.append(i)

    expanded_columns = []
    lines = list(np.swapaxes(lines, 0, 1))
    # Get indices of columns that should be expanded
    for i in range(len(lines)):
        if all([x == '.' for x in lines[i]]):
            expanded_columns.append(i)

    lines = list(np.swapaxes(lines, 0, 1))

    # Store locations of galaxies
    galaxy_locations = []
    for y in range(len(lines)):
        for x in range(len(lines[y])):
            if lines[y][x] == '#':
                galaxy_locations.append((y, x))
        
    # Sum distances between all galaxies
    total = 0
    for galaxy_index1 in range(len(galaxy_locations)):
        for galaxy_index2 in range(galaxy_index1 + 1, len(galaxy_locations)):
            # Get vertical distance between galaxies
            row1 = galaxy_locations[galaxy_index1][0]
            row2 = galaxy_locations[galaxy_index2][0]
            min_row = min(row1, row2)
            max_row = max(row1, row2)
            row_expanded_count = 0
            for row in expanded_rows:
                # Find number of lines that should be expanded
                if row >= min_row and row <= max_row:
                    row_expanded_count += 1
            vertical_distance = row_expanded_count * expansion_factor + max_row - min_row

            # Get horizontal distance between galaxies
            col1 = galaxy_locations[galaxy_index1][1]
            col2 = galaxy_locations[galaxy_index2][1]
            min_col = min(col1, col2)
            max_col = max(col1, col2)
            col_expanded_count = 0
            for col in expanded_columns:
                # Find number of lines that should be expanded
                if col >= min_col and col <= max_col:
                    col_expanded_count += 1
            horizontal_distance = col_expanded_count * expansion_factor + max_col - min_col

            total += vertical_distance + horizontal_distance

    print(total)
    # Time: 13:25:00
    # Rank: 29045

if __name__ == '__main__':
    part1()
    part2()