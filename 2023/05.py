'''https://adventofcode.com/2023/day/5'''

import utils

map_indices = [(3, 26), (28, 37), (39, 59), (61, 101), (103, 139), (141, 176), (178, 204)]

def part1():
    lines = utils.get_lines("inputs/05.txt")
    seeds = [int(x) for x in lines[0].split(": ")[1].split(" ")]

    for map_index in map_indices:
        mapping = []
        # Parse mapping for this mapping step
        for i in range(map_index[0], map_index[1]):
            mapping.append([int(x) for x in lines[i].split(" ")])

        for i in range(len(seeds)):
            # Map each seed if the seed is in the correct range.
            for map in mapping:
                if seeds[i] >= map[1] and seeds[i] <= map[1] + map[2]:
                    seeds[i] = seeds[i] - map[1] + map[0]
                    break
    
    print(min(seeds))
    # Time: 07:14:07
    # Rank: 32269

def part2():
    lines = utils.get_lines("inputs/05.txt")

    # Seeds is a collection of two-lists, with index 0 the start and index 1 the size
    seeds_raw = [int(x) for x in lines[0].split(": ")[1].split(" ")]
    seeds = []
    for i in range(0, len(seeds_raw)):
        if i % 2 == 0:
            seeds.append([seeds_raw[i]])
        else:
            seeds[-1].append(seeds_raw[i])
        
    map_indices.reverse()
    full_max = 4294967296

    # Parse all mappings
    mappings = []
    for map_index in map_indices:
        mapping = []
        for i in range(map_index[0], map_index[1]):
            temp = lines[i].split(" ")
            mapping.append([int(x) for x in temp])
        mappings.append(mapping)

    # Strategy is a reverse brute force: 
    # try all possible location values in order, map backwards.
    # If the final value is a valid seed, then we found the minimum location.
    for check_value in range(full_max):
        mapped_value = check_value
        for mapping in mappings:
            for map in mapping:
                if mapped_value >= map[0] and mapped_value <= map[0] + map[2]:
                    mapped_value = mapped_value - map[0] + map[1]
                    break
        for seed in seeds:          
            if mapped_value >= seed[0] and mapped_value <= seed[0] + seed[1]:
                print(check_value)
                return
    
    print("No minimum found")
    # Time: 19:34:38
    # Rank: 37925

if __name__ == '__main__':
    part1()
    part2()