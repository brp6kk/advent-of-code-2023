'''https://adventofcode.com/2023/day/12'''

import utils

def update_cache(cache, key, value):
    cache[key] = value
    return value

def count_valid_arragements_memo(springs, records, cache):
    key = (springs, tuple(records))

    # Check if this result has alredy been computed for this state
    if key in cache.keys():
        return cache[key]
    
    if len(springs) == 0 and len(records) == 0:
        # If we get through all the springs and records, we have a valid configuration.
        return update_cache(cache, key, 1)
    if len(springs) == 0:
        # There still remain some records despite there being no more springs, which means the config is invalid
        return update_cache(cache, key, 0)
    
    if springs[0] == '?':
        # Try arrangement where ? is replaced with unbroken & broken springs
        return update_cache(cache, key, 
                            count_valid_arragements_memo('.' + springs[1:], records, cache) + count_valid_arragements_memo('#' + springs[1:], records, cache))
    if springs[0] == '.':
        # Since we want to check if broken springs match records, simply discard non-broken springs
        return update_cache(cache, key, count_valid_arragements_memo(springs[1:], records, cache))
    
    # At this point, we know springs[0] must equal a broken spring
    is_invalid_arrangement = (len(records) == 0 or # Records claims there should be no more broken
        len(springs) < records[0] or # Not enough springs to possibly have the correct amount
        '.' in springs[0:records[0]] or # Non-broken spring where records claims there should be broken
        (len(springs) > records[0] and '#' == springs[records[0]])) # Broken spring where records claims there should be non-broken
    if is_invalid_arrangement:
        return update_cache(cache, key, 0)
    
    if len(springs) == records[0]:
        # No more springs, but still need to check base case
        return update_cache(cache, key, count_valid_arragements_memo('', records[1:], cache))
    
    # After removing valid section of broken springs, check remainder of springs
    return update_cache(cache, key, count_valid_arragements_memo('.' + springs[1 + records[0]:], records[1:], cache))

def part1():
    lines = utils.get_lines("inputs/12.txt")

    count = 0
    cache = dict()
    for line in lines:
        springs, raw_records = line.split()
        records = utils.line_to_int_list(raw_records, ",")
        count += count_valid_arragements_memo(springs, records, cache)
    
    print(count)
    # Time: 07:25:36
    # Rank: 15506

def part2():
    lines = utils.get_lines("inputs/12.txt")
    count = 0
    cache = dict()
    for line in lines:
        springs, raw_records = line.split()
        records = utils.line_to_int_list(raw_records, ",")

        springs = ((springs + "?") * 5)[:-1]
        records = records * 5

        count += count_valid_arragements_memo(springs, records, cache)
    
    print(count)
    # Time: 09:36:23
    # Rank: 7786

if __name__ == '__main__':
    part1()
    part2()