'''https://adventofcode.com/2023/day/2'''

import utils

def part1():
    lines = utils.get_lines("inputs/02.txt")
    color_max = {
        "red": 12,
        "green": 13,
        "blue": 14
    }
    impossible_days = []

    for line_index in range(100):
        # For each line (containing multiple games), determine if there exists any impossible game
        day = line_index + 1
        impossibility_found = False
        games = lines[line_index].split(": ")[1].split("; ")
        game_index = 0
        while not impossibility_found and game_index < len(games): # Loop thru games in a line
            game = games[game_index]
            cubes = game.split(", ")
            cube_index = 0
            while not impossibility_found and cube_index < len(cubes): # Loop thru cubes in a game
                cube = cubes[cube_index]
                num = int(''.join(x for x in cube if x.isdigit()))
                for color, max_count_of_color in color_max.items():
                    if color in cube and num > max_count_of_color:
                        impossible_days.append(day)
                        impossibility_found = True
                        break
                cube_index += 1
            game_index += 1

    possible_days = [x for x in range(1, 101) if x not in impossible_days]
    print(sum(possible_days))
    # Time: 07:08:58
    # Rank: 45017

def part2():
    lines = utils.get_lines("inputs/02.txt")

    final_sum = 0
    for line in lines:
        games = line.split(": ")[1].split("; ")

        # The minimum possible number of cubes is the maximum cube count observed
        maximums = {
            "red": 0,
            "green": 0,
            "blue": 0
        }
        for game in games:
            for cube in game.split(", "):
                num = int(''.join(x for x in cube if x.isdigit()))
                for color, current_max in maximums.items():
                    if color in cube and num > current_max:
                        maximums[color] = num
                        break

        final_sum += maximums["red"] * maximums["blue"] * maximums["green"]

    print(final_sum)
    # Time: 07:15:09
    # Rank: 41824

if __name__ == '__main__':
    part1()
    part2()