'''https://adventofcode.com/2023/day/25'''

import utils
import networkx as nx

def part1():
    lines = utils.get_lines("inputs/25.txt")
    edges = []
    for line in lines:
        source, destination_str = line.strip().split(": ")
        destinations = destination_str.split()
        for d in destinations:
            edges.append((source, d))

    g = nx.Graph()
    g.add_edges_from(edges)
    # Edges that separate g into two disconnected graphs
    cutset = nx.minimum_edge_cut(g)
    g.remove_edges_from(cutset)
    # Get the disconnected graphs as separate graphs
    subgraphs = [g.subgraph(c) for c in nx.connected_components(g)]

    prod = 1
    for sg in subgraphs:
        prod *= sg.number_of_nodes()

    print(prod)
    # Time: 10:39:27
    # Rank: 6526

def part2():
    pass

if __name__ == '__main__':
    part1()
    part2()