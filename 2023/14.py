'''https://adventofcode.com/2023/day/14'''

import utils

def part1():
    lines = utils.get_lines("inputs/14.txt")
    lines = [x.strip() for x in lines]

    total = 0
    rock_count = [0] * len(lines[0])
    for y in range(len(lines) - 1, -1, -1):
        # Move south -> north
        load = len(lines) - y
        for x in range(len(lines[y])):
            if lines[y][x] == '#':
                # Add up the load for the rocks that stop below here & reset counter
                for i in range(rock_count[x]):
                    total += load - i - 1
                rock_count[x] = 0
            elif lines[y][x] == 'O':
                rock_count[x] += 1

    # Final summing for rocks that stop at the north edge
    for x in range(len(lines[0])):
        for i in range(rock_count[x]):
            total += len(lines) - i 

    print(total)
    # Time: 07:14:42
    # Rank: 18634

def tilt_north(grid):
    '''Move round rocks as far north as possible.'''
    for y in range(len(grid)):
        for x in range(len(grid[y])):
            if grid[y][x] == 'O':
                move_y = y - 1
                while (move_y >= 0 and grid[move_y][x] == '.'):
                    grid[move_y][x] = 'O'
                    grid[move_y + 1][x] = '.'
                    move_y -= 1    

def tilt_south(grid):
    '''Move round rocks as far south as possible.'''
    for y in range(len(grid) - 1, -1, -1):
        for x in range(len(grid[y])):
            if grid[y][x] == 'O':
                move_y = y + 1
                while (move_y < len(grid) and grid[move_y][x] == '.'):
                    grid[move_y][x] = 'O'
                    grid[move_y - 1][x] = '.'
                    move_y += 1

def tilt_west(grid):
    '''Move round rocks as far west as possible.'''
    for y in range(len(grid)):
        for x in range(len(grid[y])):
            if grid[y][x] == 'O':
                move_x = x - 1
                while (move_x >= 0 and grid[y][move_x] == '.'):
                    grid[y][move_x] = 'O'
                    grid[y][move_x + 1] = '.'
                    move_x -= 1

def tilt_east(grid):
    '''Move round rocks as far east as possible.'''
    for y in range(len(grid)):
        for x in range(len(grid[y]) - 1, -1, -1):
            if grid[y][x] == 'O':
                move_x = x + 1
                while (move_x < len(grid[y]) and grid[y][move_x] == '.'):
                    grid[y][move_x] = 'O'
                    grid[y][move_x - 1] = '.'
                    move_x += 1

def calculate_total_load(grid):
    '''Calculate load, as defined in problem statement.'''
    total = 0
    for (y, load) in zip(range(len(grid)), range(len(grid), 0, -1)):
        for x in range(len(grid[y])):
            if grid[y][x] == 'O':
                total += load
    return total   

def part2():
    lines = utils.get_lines("inputs/14.txt")
    lines = [list(x.strip()) for x in lines]

    for _ in range(1000):
        # Problem says to go to 1000000000, but I chose to assume it loops eventually.
        # This was a correct assumption.
        # I also chose to write separate functions for different directions, rather than trying to flip/transpose,
        # since I didn't want to wait through the extra runtime that those operations would require.
        tilt_north(lines)
        tilt_west(lines)
        tilt_south(lines)
        tilt_east(lines)
    
    print(calculate_total_load(lines))
    # Time: 13:14:51
    # Rank: 17532

if __name__ == '__main__':
    part1()
    part2()