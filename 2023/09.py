'''https://adventofcode.com/2023/day/9'''

import utils

def part1():
    lines = utils.get_lines("inputs/09.txt")
    extrapolated_values = []

    for line in lines:
        line = utils.line_to_int_list(line)
        all_differences = [line]

        # Find differences between original elements, differences of those differences, etc.
        while not all([x == 0 for x in all_differences[-1]]):
            all_differences.append([all_differences[-1][i] - all_differences[-1][i - 1] for i in range(1, len(all_differences[-1]))])
        
        # Extrapolate next differences
        for i in range(len(all_differences) - 2, -1, -1):
            all_differences[i].append(all_differences[i + 1][-1] + all_differences[i][-1])

        extrapolated_values.append(all_differences[0][-1])

    print(sum(extrapolated_values))
    # Time: 07:10:10
    # Rank: 27924

def part2():
    lines = utils.get_lines("inputs/09.txt")
    extrapolated_values = []

    for line in lines:
        line = utils.line_to_int_list(line)
        all_differences = [line]

        # Find differences between original elements, differences of those differences, etc.
        while not all([x == 0 for x in all_differences[-1]]):
            all_differences.append([all_differences[-1][i] - all_differences[-1][i - 1] for i in range(1, len(all_differences[-1]))])
        
        # Extrapolate previous differences
        for i in range(len(all_differences) - 2, -1, -1):
            all_differences[i].insert(0, all_differences[i][0] - all_differences[i + 1][0])

        extrapolated_values.append(all_differences[0][0])

    print(sum(extrapolated_values))
    # Time: 07:14:46
    # Rank: 26912

if __name__ == '__main__':
    part1()
    part2()