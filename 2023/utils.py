def get_lines(fname):
    with open(fname, 'r') as fp:
        lines = fp.readlines()
    return lines

def line_to_int_list(line, delim=None):
    if delim:
        return [int(x) for x in line.split(delim)]
    return [int(x) for x in line.split()]    