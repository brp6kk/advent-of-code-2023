'''https://adventofcode.com/2023/day/3'''

import utils

class Gear():
    def __init__(self, count, product):
        self.count = count
        self.product = product

def is_symbol(x):
    return not (x == '.' or x.isalpha() or x.isnumeric())

def is_gear(x): 
    return x == '*'

def part1():
    lines = utils.get_lines("inputs/03.txt")

    sum = 0
    for i in range(len(lines)):
        # Remove trailing newlines
        if i < len(lines):
            lines[i] = lines[i][:-2]

        is_parsing = False
        is_valid = False
        num = ""
        for j in range(len(lines[i])):
            if not is_parsing and lines[i][j].isnumeric():
                # Number first encountered
                is_parsing = True
            elif is_parsing and not lines[i][j].isnumeric():
                # Done parsing number.
                if is_valid:
                    int_num = int(num)
                    sum += int_num
                num = ""
                is_valid = False
                is_parsing = False

            if is_parsing:
                num += lines[i][j]

                # Check surrounding elements. 
                # A number must have some digit next to a symbol to be valid.
                if i > 0:
                    if j > 0:
                        is_valid = is_valid or is_symbol(lines[i - 1][j - 1])
                    is_valid = is_valid or is_symbol(lines[i - 1][j])
                    if j < len(lines[i - 1]) - 1:
                        is_valid = is_valid or is_symbol(lines[i - 1][j + 1])
                
                if j > 0:
                    is_valid = is_valid or is_symbol(lines[i][j - 1])
                if j < len(lines[i]) - 1:
                    is_valid = is_valid or is_symbol(lines[i][j + 1])
                
                if i < len(lines) - 1:
                    if j > 0:
                        is_valid = is_valid or is_symbol(lines[i + 1][j - 1])
                    is_valid = is_valid or is_symbol(lines[i + 1][j])
                    if j < len(lines[i + 1]) - 1:
                        is_valid = is_valid or is_symbol(lines[i + 1][j + 1])

        if is_parsing and is_valid:
            int_num = int(num)
            sum += int_num

    print(sum)
    # Time: 09:58:56
    # Rank: 45275

def part2():
    lines = utils.get_lines("inputs/03.txt")
    gears = dict()
    
    for i in range(len(lines)):
        # Remove trailing newlines
        if i < len(lines):
            lines[i] = lines[i][:-2]

        is_parsing = False
        is_valid = False
        num = ""
        adjacent_gears = set()
        
        for j in range(len(lines[i])):
            if not is_parsing and lines[i][j].isnumeric():
                # Number first encountered.
                is_parsing = True

            elif is_parsing and not lines[i][j].isnumeric():
                # Done parsing number.
                if is_valid:
                    int_num = int(num)
                    for gear in adjacent_gears:
                        if gear not in gears.keys():
                            gears[gear] = Gear(1, int_num)
                        else:
                            gears[gear].count += 1
                            gears[gear].product *= int_num
                num = ""
                is_valid = False
                is_parsing = False
                adjacent_gears = set()

            if is_parsing:
                num += lines[i][j]

                # Check surrounding elements. 
                # A number must have some digit next to a symbol to be valid.
                # Also need to keep track of which gears have which numbers in contact
                if i > 0:
                    if j > 0:
                        is_valid = is_valid or is_symbol(lines[i - 1][j - 1])
                        if is_gear(lines[i - 1][j - 1]):
                            adjacent_gears.add((i - 1, j - 1))
                    is_valid = is_valid or is_symbol(lines[i - 1][j])
                    if is_gear(lines[i - 1][j]):
                        adjacent_gears.add((i - 1, j))
                    if j < len(lines[i - 1]) - 1:
                        is_valid = is_valid or is_symbol(lines[i - 1][j + 1])
                        if is_gear(lines[i - 1][j + 1]):
                            adjacent_gears.add((i - 1, j + 1))
                
                if j > 0:
                    is_valid = is_valid or is_symbol(lines[i][j - 1])
                    if is_gear(lines[i][j - 1]):
                        adjacent_gears.add((i, j - 1))
                if j < len(lines[i]) - 1:
                    is_valid = is_valid or is_symbol(lines[i][j + 1])
                    if is_gear(lines[i][j + 1]):
                        adjacent_gears.add((i, j + 1))
                
                if i < len(lines) - 1:
                    if j > 0:
                        is_valid = is_valid or is_symbol(lines[i + 1][j - 1])
                        if is_gear(lines[i + 1][j - 1]):
                            adjacent_gears.add((i + 1, j - 1))
                    is_valid = is_valid or is_symbol(lines[i + 1][j])
                    if is_gear(lines[i + 1][j]):
                        adjacent_gears.add((i + 1, j))
                    if j < len(lines[i + 1]) - 1:
                        is_valid = is_valid or is_symbol(lines[i + 1][j + 1])
                        if is_gear(lines[i + 1][j + 1]):
                            adjacent_gears.add((i + 1, j + 1))

        if is_parsing and is_valid:
            int_num = int(num)
            for gear in adjacent_gears:
                if gear not in gears.keys():
                    gears[gear] = Gear(1, int_num)
                else:
                    gears[gear].count += 1
                    gears[gear].product *= int_num
        
    answer = sum([g.product for g in gears.values() if g.count == 2])
    print(answer)
    # Time: 14:22:15
    # Rank: 49694

if __name__ == '__main__':
    part1()
    part2()