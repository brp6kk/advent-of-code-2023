[Advent of Code website](https://adventofcode.com)

From their about section: 
> **Advent of Code** is an [Advent calendar](https://en.wikipedia.org/wiki/Advent_calendar) of small programming puzzles for a variety of skill sets and skill levels that can be solved in any programming language you like.

Code is organized so that I have on directory per year. For each year, there is one code file per day.
