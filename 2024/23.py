'''https://adventofcode.com/2024/day/23'''

import utils
import networkx as nx

def get_graph():
    lines = utils.get_lines("inputs/23.txt", True)

    edges = [line.split("-") for line in lines]

    g = nx.Graph()
    g.add_edges_from(edges)
    return g

def part1():
    g = get_graph()
    cycles = nx.simple_cycles(g, 3)
    count = 0
    for c in cycles:
        for item in c:
            if item[0] == 't': 
                count += 1
                break
    print(count)
    # Time: 10:55:22
    # Rank: 15316

def part2():
    g = get_graph()
    cliques = nx.find_cliques(g)
    largest_clique = max(cliques, key=lambda x: len(x))
    largest_clique.sort()
    print(",".join(largest_clique))
    # Time: 11:07:25
    # Rank: 12567

if __name__ == "__main__":
    part1()
    part2()