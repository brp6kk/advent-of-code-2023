'''https://adventofcode.com/2024/day/4'''

import utils

def part1():
    lines = utils.get_lines('inputs/04.txt', True)
    height = len(lines)
    width = len(lines[0])
    count = 0
    for y in range(height):
        for x in range(width):
            if lines[y][x] != 'X': continue
            count += ((y >= 3 and lines[y-1][x] == 'M' and lines[y-2][x] == 'A' and lines[y-3][x] == 'S') +
                      (y <= height - 4 and lines[y+1][x] == 'M' and lines[y+2][x] == 'A' and lines[y+3][x] == 'S') +
                      (x >= 3 and lines[y][x-1] == 'M' and lines[y][x-2] == 'A' and lines[y][x-3] == 'S') +
                      (x <= width - 4 and lines[y][x+1] == 'M' and lines[y][x+2] == 'A' and lines[y][x+3] == 'S') +
                      (y >= 3 and x >= 3 and lines[y-1][x-1] == 'M' and lines[y-2][x-2] == 'A' and lines[y-3][x-3] == 'S') +
                      (y <= height - 4 and x <= width - 4 and lines[y+1][x+1] == 'M' and lines[y+2][x+2] == 'A' and lines[y+3][x+3] == 'S') +
                      (y >= 3 and x <= width - 4 and lines[y-1][x+1] == 'M' and lines[y-2][x+2] == 'A' and lines[y-3][x+3] == 'S') + 
                      (y <= width - 4 and x >= 3 and lines[y+1][x-1] == 'M' and lines[y+2][x-2] == 'A' and lines[y+3][x-3] == 'S')
            )
            
    print(count)
    # Time: 07:21:29
    # Rank: 40547 

def part2():
    lines = utils.get_lines('inputs/04.txt', True)
    height = len(lines)
    width = len(lines[0])
    count = 0
    for y in range(1,height-1):
        for x in range(1,width-1):
            if lines[y][x] != 'A': continue
            count += (((lines[y-1][x-1] == 'M' and lines[y+1][x+1] == 'S') or (lines[y-1][x-1] == 'S' and lines[y+1][x+1] == 'M')) and
                      ((lines[y-1][x+1] == 'M' and lines[y+1][x-1] == 'S') or (lines[y-1][x+1] == 'S' and lines[y+1][x-1] == 'M'))
            )

    print(count)
    # Time: 07:39:55  
    # Rank: 35087

if __name__ == '__main__':
    part1()
    part2()