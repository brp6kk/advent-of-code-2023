'''https://adventofcode.com/2024/day/2'''

import utils

def is_report_safe(levels):
    is_increasing = levels[1] > levels[0]
    for i in range(len(levels) - 1):
        if ((is_increasing and (levels[i + 1] - levels[i] > 3 or levels[i + 1] - levels[i] < 1)) or
                (not is_increasing and (levels[i] - levels[i + 1] > 3 or levels[i] - levels[i + 1] < 1))):
            return False
    return True

def part1():
    lines = utils.get_lines('inputs/02.txt')
    num_safe = 0
    for line in lines:
        levels = utils.line_to_int_list(line)
        num_safe += is_report_safe(levels)
    print(num_safe)
    # Time: 07:11:38
    # Rank: 52622

def part2():
    lines = utils.get_lines('inputs/02.txt')
    num_safe = 0
    for line in lines:
        levels = utils.line_to_int_list(line)
        is_safe = is_report_safe(levels)
        if not is_safe:
            for i in range(len(levels)):
                if is_report_safe(levels[0:i] + levels[i+1:]):
                    is_safe = True
                    break
        num_safe += is_safe
    print(num_safe)
    # Time: 07:17:32
    # Rank: 36342

if __name__ == '__main__':
    part1()
    part2()