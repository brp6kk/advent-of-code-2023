'''https://adventofcode.com/2024/day/14'''

import utils
from utils import Point
import re
import functools

WIDTH = 101
HEIGHT = 103
REGEX = "(?:p=)(\d+),(\d+)(?: v=)(-?\d+),(-?\d+)"

class Robot:
    def __init__(self, y, x, vy, vx):
        self.y = y
        self.x = x
        self.vy = vy
        self.vx = vx

def find_quadrant(x, y,):
    try:
        return [
            x < WIDTH // 2 and y < HEIGHT // 2,
            x < WIDTH // 2 and y > HEIGHT // 2,
            x > WIDTH // 2 and y < HEIGHT // 2,
            x > WIDTH // 2 and y > HEIGHT // 2
        ].index(True)
    except:
        return None

def part1():
    lines = utils.get_lines('inputs/14.txt', True)
    quadrant_counts = [0, 0, 0, 0]
    for line in lines:
        x, y, vx, vy = [int(item) for item in re.findall(REGEX, line)[0]]
        finalx = (x + vx * 100) % WIDTH
        finaly = (y + vy * 100) % HEIGHT
        quadrant = find_quadrant(finalx, finaly)
        if quadrant is not None:
            quadrant_counts[quadrant] += 1
        
    print(functools.reduce(lambda a, b: a * b, quadrant_counts))
    # Time: 10:09:02
    # Rank: 23445

def part2():
    lines = utils.get_lines('inputs/14.txt', True)
    robots = []
    for line in lines:
        x, y, vx, vy = [int(item) for item in re.findall(REGEX, line)[0]]
        robots.append(Robot(y, x, vy, vx))

    i = 1
    while True:
        locs = set()
        for robot in robots:
            pos = Point((robot.y + robot.vy * i) % HEIGHT, (robot.x + robot.vx * i) % WIDTH)
            locs.add(pos)

        # Assume tree shape when all robots have unique location
        if len(locs) == len(robots):
            break
        i += 1

    print(i)
    # Time: 15:56:53
    # Rank: 24723

if __name__ == '__main__':
    part1()
    part2()