'''https://adventofcode.com/2024/day/15'''

import utils
from utils import Point

ROBOT = '@'
BOX = 'O'
WALL = '#'
EMPTY = '.'
BOX_LEFT = '['
BOX_RIGHT = ']'
WIDE_BOXES = f'{BOX_LEFT}{BOX_RIGHT}'

LEFT = Point(0, -1)
RIGHT = Point(0, 1)
UP = Point(-1, 0)
DOWN = Point(1, 0)

def find_robot(map):
    for y in range(len(map)):
        for x in range(len(map[y])):
            if map[y][x] == ROBOT:
                return Point(y, x)
    return None

def translate_movement(movement):
    symbols = '^v<>'
    points = [UP, DOWN, LEFT, RIGHT]
    return points[symbols.index(movement)]

def get_starting_map_movements():
    lines = utils.get_lines('inputs/15.txt', True)
    break_line = lines.index('')

    map = lines[:break_line]
    for i in range(len(map)):
        map[i] = list(map[i])

    movements = lines[break_line + 1:]
    movements = "".join(movements)

    return map, movements

def calculate_gps(map, marker):
    gps = 0
    for y in range(len(map)):
        for x in range(len(map[y])):
            if map[y][x] == marker:
                gps += y * 100 + x
    return gps

def is_able_part2_move(map, current, dir):
    next = current + dir
    next = dir * Point(1, 2) + current if map[next.y][next.x] in WIDE_BOXES else next

    next_map_item = map[next.y][next.x]
    if next_map_item == WALL: return False
    if next_map_item == EMPTY: return True

    # verify boxes can move in dir
    can_move_forward = is_able_part2_move(map, next, dir)
    if dir.y == 0:
        return can_move_forward
    if next_map_item == BOX_LEFT:
        return can_move_forward and is_able_part2_move(map, next + RIGHT, dir)
    return can_move_forward and is_able_part2_move(map, next + LEFT, dir)

def part2_move(map, current, dir):
    cur_map_item = map[current.y][current.x]
    if cur_map_item == EMPTY: return
    next = dir * Point(0, 2) + current if dir.y == 0 else dir + current
    next_map_item = map[next.y][next.x]
    if next_map_item == WALL: return

    if dir == RIGHT:
        if cur_map_item == BOX_LEFT and next_map_item == BOX_LEFT:
            part2_move(map, next, dir)
        if cur_map_item == BOX_LEFT: # move current regardless of next (since if next exists, it was just moved)
            map[next.y][next.x] = BOX_RIGHT
            map[current.y][current.x] = EMPTY
            map[current.y][current.x + 1] = BOX_LEFT
        return
    elif dir == LEFT:
        if cur_map_item == BOX_RIGHT and next_map_item == BOX_RIGHT:
            part2_move(map, next, dir)
        if cur_map_item == BOX_RIGHT: # move current regardless of next (since if next exists, it was just moved)
            map[next.y][next.x] = BOX_LEFT
            map[current.y][current.x] = EMPTY
            map[current.y][current.x - 1] = BOX_RIGHT
        return
    
    # UP or DOWN
    next_left = next + LEFT
    next_right = next + RIGHT

    # move boxes that are directly in front of current spot
    if cur_map_item in WIDE_BOXES and next_map_item in WIDE_BOXES:
        part2_move(map, next, dir)
        if cur_map_item == BOX_RIGHT and next_map_item == BOX_LEFT:
            part2_move(map, next_left, dir)
        if cur_map_item == BOX_LEFT and next_map_item == BOX_RIGHT:
            part2_move(map, next_right, dir)

    # might also be boxes that are in front but to the side a bit
    if cur_map_item == BOX_LEFT and map[next_right.y][next_right.x] == BOX_LEFT:
        part2_move(map, next_right, dir)
    elif cur_map_item == BOX_RIGHT and map[next_left.y][next_left.x] == BOX_RIGHT:
        part2_move(map, next_left, dir)

    # update map for current item
    if cur_map_item in WIDE_BOXES:
        map[next.y][next.x] = map[current.y][current.x]
    if cur_map_item == BOX_LEFT:
        map[next.y][next.x + 1] = BOX_RIGHT
        map[current.y][current.x + 1] = EMPTY
    elif cur_map_item == BOX_RIGHT:
        map[next.y][next.x - 1] = BOX_LEFT
        map[current.y][current.x - 1] = EMPTY

    map[current.y][current.x] = EMPTY

def part1():
    map, movements = get_starting_map_movements()
    robot = find_robot(map)
    map[robot.y][robot.x] = EMPTY
    
    for str_movement in movements:
        movement = translate_movement(str_movement)
        next_point = robot + movement
        if map[next_point.y][next_point.x] == EMPTY:
            robot = next_point
        if map[next_point.y][next_point.x] != BOX: # wall or empty, don't need to simulate box pushes
            continue

        iter_point = next_point
        while map[iter_point.y][iter_point.x] == BOX:
            iter_point = iter_point + movement
        if map[iter_point.y][iter_point.x] == EMPTY:
            map[iter_point.y][iter_point.x] = BOX
            map[next_point.y][next_point.x] = EMPTY
            robot = next_point
            
    print(calculate_gps(map, BOX))
    # Time: 15:45:31
    # Rank: 26824

def part2():
    orig_map, movements = get_starting_map_movements()
    # expand map
    map = []
    for orig_row in orig_map:
        row = []
        for orig_cell in orig_row:
            row.extend([[WALL,WALL], [BOX_LEFT,BOX_RIGHT], [EMPTY,EMPTY], [ROBOT,EMPTY]][f'{WALL}{BOX}{EMPTY}{ROBOT}'.index(orig_cell)])
        map.append(row)

    robot = find_robot(map)
    map[robot.y][robot.x] = EMPTY

    for str_movement in movements:
        movement = translate_movement(str_movement)
        next = robot + movement
        if not is_able_part2_move(map, robot, movement): 
            continue
        part2_move(map, next, movement)

        robot = next

    print(calculate_gps(map, BOX_LEFT))
    # Time: 21:43:56
    # Rank: 20745

if __name__ == '__main__':
    part1()
    part2()