'''https://adventofcode.com/2024/day/25'''

import utils

FILLED = '#'

def get_heights(lines):
    result = [0] * len(lines[0])
    for y in range(len(lines)):
        for x in range(len(lines[y])):
            result[x] += lines[y][x] == FILLED
    return result

def part1():
    lines = utils.get_lines("inputs/25.txt", True)
    lines.append('')

    # Parse keys & locks from lines, where each is a list of pin heights
    keys = []
    locks = []
    accumulator = []
    for line in lines:
        if line != "":
            accumulator.append(line)
            continue
        heights = get_heights(accumulator)
        if accumulator[0][0] == FILLED:
            locks.append(heights)
        else:
            keys.append(heights)
        accumulator.clear()

    count = sum([sum([not any([x[0] + x[1] > 7 for x in zip(key, lock)]) for key in keys]) for lock in locks])
    print(count)
    # Time: 07:43:11
    # Rank: 12420

def part2():
    lines = utils.get_lines("inputs/25.txt", True)

if __name__ == '__main__':
    part1()
    part2()