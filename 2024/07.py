'''https://adventofcode.com/2024/day/7'''

import utils
import itertools

def solve(operators):
    lines = utils.get_lines('inputs/07.txt')
    total = 0

    for line in lines:
        a, b = line.split(": ")
        test_value = int(a)
        numbers = utils.line_to_int_list(b)

        for p in list(itertools.product(operators, repeat=len(numbers)-1)):
            test_total = numbers[0]
            for i in range(len(p)):
                if p[i] == '+':
                    test_total += numbers[i+1]
                elif p[i] == "*":
                    test_total *= numbers[i+1]
                elif p[i] == '|':
                    test_total = int(str(test_total) + str(numbers[i+1]))

            if test_total == test_value:
                total += test_value
                break

    return total

def part1():
    print(solve("*+"))
    # Time: 07:11:54
    # Rank: 27534

def part2():
    print(solve("*+|"))
    # Time: 07:30:34
    # Rank: 25462

if __name__ == '__main__':
    part1()
    part2()