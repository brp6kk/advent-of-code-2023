'''https://adventofcode.com/2024/day/1'''

import utils

def get_lists():
    lines = utils.get_lines('inputs/01.txt')
    list1 = []
    list2 = []
    for line in lines:
        a, b = line.split("   ")
        list1.append(int(a))
        list2.append(int(b))  
    return list1, list2

def part1():
    list1, list2 = get_lists()
    list1.sort()
    list2.sort()

    total_distance = 0
    for i in range(len(list1)):
        total_distance += abs(list1[i] - list2[i])
    print(total_distance)
    # Time: 06:59:07
    # Rank: 38940

def part2():
    list1, list2 = get_lists()

    # Lookup of counts - how often does an ID appear in list2
    list2_lookup = dict()
    for item in list2:
        if item in list2_lookup.keys():
            list2_lookup[item] += 1
        else:
            list2_lookup[item] = 1
    
    score = 0
    for item in list1:
        multiplier = list2_lookup[item] if item in list2_lookup.keys() else 0
        score += item * multiplier
    print(score)
    # Time: 07:05:11
    # Rank: 36099

if __name__ == '__main__':
    part1()
    part2()