'''https://adventofcode.com/2024/day/22'''

import utils

class FourSequence:
    def __init__(self, one=None, two=None, three=None, four=None):
        self.one = one
        self.two = two 
        self.three = three
        self.four = four

    def __eq__(self, other):
        return (isinstance(other, FourSequence) and other.one == self.one and 
                other.two == self.two and other.three == self.three and other.four == self.four)
    
    def __str__(self):
        return f"({self.one}, {self.two}, {self.three}, {self.four})"
    
    def __hash__(self):
        return hash(str(self))
    
    def next_sequence(self, next):
        return FourSequence(self.two, self.three, self.four, next)
    
    def is_valid(self):
        return None not in [self.one, self.two, self.three, self.four]

def take_step(num):
    mix_prune = lambda x, y: (x ^ y) % 16777216
    step1 = mix_prune((num * 64), num)
    step2 = mix_prune(int(step1 / 32), step1)
    step3 = mix_prune((step2 * 2048), step2)
    return step3

def part1():
    lines = utils.get_lines("inputs/22.txt", True)
    total = 0
    for num in lines:
        num = int(num)
        for _ in range(2000):
            num = take_step(num)
        total += num
    print(total)
    # Time: 07:15:50
    # Rank: 12426

def part2():
    lines = utils.get_lines("inputs/22.txt", True)
    sequences = dict() # sequences[four price change sequence] = total bananas when selling
    seen = set() # seen sequences for a given secret number (reset in foreach)
    for num in lines:
        seen.clear()
        latest_sequence = FourSequence()
        num = int(num)
        for _ in range(2000):
            next_num = take_step(num)
            digit = num % 10
            next_digit = next_num % 10

            next_seq = latest_sequence.next_sequence(next_digit - digit)
            if next_seq.is_valid() and next_seq not in seen:
                sequences[next_seq] = (0 if next_seq not in sequences.keys() else sequences[next_seq]) + next_digit
                seen.add(next_seq)

            latest_sequence = next_seq
            num = next_num

    print(max(sequences.values()))
    # Time: 11:58:09
    # Rank: 12187

if __name__ == "__main__":
    part1()
    part2()