'''https://adventofcode.com/2024/day/13'''

import utils
import re

def find_num_tokens(prize_position_offset=0):
    # https://en.wikipedia.org/wiki/Cramer%27s_rule#Explicit_formulas_for_small_systems
    lines = utils.get_lines('inputs/13.txt', True)
    i = 0
    token = 0
    while i < len(lines):
        x1, y1 = [int(item) for item in re.findall("(?:X\+)(\d+)(?:, Y\+)(\d+)", lines[i])[0]]
        x2, y2 = [int(item) for item in re.findall("(?:X\+)(\d+)(?:, Y\+)(\d+)", lines[i + 1])[0]]
        x3, y3 = [int(item) for item in re.findall("(?:X\=)(\d+)(?:, Y\=)(\d+)", lines[i + 2])[0]]
        x3 += prize_position_offset
        y3 += prize_position_offset

        det = x1 * y2 - x2 * y1
        a = (x3 * y2 - x2 * y3) // det
        b = (x1 * y3 - x3 * y1) // det
        if x1 * a + x2 * b == x3 and y1 * a + y2 * b == y3:
            token += 3 * a + b

        i += 4

    return token

def part1():
    print(find_num_tokens())
    # Time: 07:43:23
    # Rank: 20558

def part2():
    print(find_num_tokens(10000000000000))
    # Time: 07:45:16
    # Rank: 15260

if __name__ == '__main__':
    part1()
    part2()