'''https://adventofcode.com/2024/day/6'''

import utils
from utils import Point

def find_start_location(lines):
    for y in range(len(lines)):
        for x in range(len(lines[y])):
            if lines[y][x] == '^': return Point(y, x)
    return Point(-1, -1)

def turn(current_dir):
    # Clockwise turn
    x = [Point(1, 0), Point(0, -1), Point(-1, 0), Point(0, 1)]
    return x[(x.index(current_dir) + 1) % 4]

def get_visited_locations(lines):
    visited = set()
    current_location = find_start_location(lines)
    current_dir = Point(-1, 0)
    while current_location.in_bounds(lines):
        visited.add(current_location)
        next_location = current_location + current_dir
        if next_location.in_bounds(lines) and lines[next_location.y][next_location.x] == '#':
            current_dir = turn(current_dir)
        else:
            current_location = next_location
    return visited

def has_cycle(lines, blocker):
    visited = dict()
    current_location = find_start_location(lines)
    current_dir = Point(-1, 0)
    while current_location.in_bounds(lines):
        if current_location in visited.keys():
            # Loop exists if approaching same location from same direction
            if current_dir in visited[current_location]: return True
            visited[current_location].add(current_dir)
        else:
            visited[current_location] = {current_dir}

        next_location = current_location + current_dir
        if next_location.in_bounds(lines) and (lines[next_location.y][next_location.x] == '#' or next_location == blocker):
            current_dir = turn(current_dir)
        else:
            current_location = next_location
    return False

def part1():
    lines = utils.get_lines('inputs/06.txt', True)
    visited = get_visited_locations(lines)
    print(len(visited))
    # Time: 07:22:46
    # Rank: 35759 

def part2():
    lines = utils.get_lines('inputs/06.txt', True)
    visited = get_visited_locations(lines)
    count = sum([has_cycle(lines, point) for point in visited])
    print(count)
    # Time: 11:43:01
    # Rank: 28513

if __name__ == '__main__':
    part1()
    part2()