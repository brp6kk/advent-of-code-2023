'''https://adventofcode.com/2024/day/3'''

import utils
import re

def part1():
    lines = utils.get_lines('inputs/03.txt')
    total = 0
    for line in lines:
        commands = re.findall("(?:mul\()(\d+),(\d+)\)", line)
        total+= sum([(int(x[0]) * int(x[1])) for x in commands])   
    print(total)
    # Time: 07:04:24
    # Rank: 53383

def part2():
    lines = "".join(utils.get_lines('inputs/03.txt'))

    dont_split = lines.split("don't()")
    enabled = dont_split[0]
    for string in dont_split[1:]:
        do_split = string.split("do()")
        enabled += "".join(do_split[1:])
        
    commands = re.findall("(?:mul\()(\d+),(\d+)\)", enabled)
    total = sum([(int(x[0]) * int(x[1])) for x in commands])
    print(total)
    # Time: 07:19:02
    # Rank: 44188

if __name__ == '__main__':
    part1()
    part2()