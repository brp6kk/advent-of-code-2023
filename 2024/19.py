'''https://adventofcode.com/2024/day/19'''

import utils

def indexof(str, substr):
    try:
        return str.index(substr)
    except ValueError:
        return -1

def check_pattern(pattern, towels, cache):
    if pattern == '': return 1
    if pattern in cache: return cache[pattern]

    valid_towels = list(filter(lambda t: indexof(pattern, t) == 0, towels))
    number_combinations = sum([check_pattern(pattern[len(vt):], towels, cache) for vt in valid_towels])
    cache[pattern] = number_combinations
    return number_combinations

def solve(addend_modifier):
    lines = utils.get_lines("inputs/19.txt", True)
    towels = lines[0].split(", ")
    patterns = lines[2:]
    cache = dict()
    return sum([addend_modifier(check_pattern(pattern, towels, cache)) for pattern in patterns])

def part1():
    print(solve(lambda x: x > 0))
    # Time: 21:34:50
    # Rank: 23348

def part2():
    print(solve(lambda x: x))
    # Time: 21:39:27
    # Rank: 20755

if __name__ == '__main__':
    part1()
    part2()