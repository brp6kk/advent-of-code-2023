'''https://adventofcode.com/2024/day/10'''

import utils
from utils import Point
import functools

dirs = [Point(1, 0), Point(0, 1), Point(-1, 0), Point(0, -1)]
lines = utils.get_lines('inputs/10.txt', True)

def recurse_score(pt, expected):
    if (not pt.in_bounds(lines)) or int(lines[pt.y][pt.x]) != expected: return set()
    if expected == 9: return {pt}

    return functools.reduce(lambda a, b: a.union(b), [recurse_score(pt + dir, expected + 1) for dir in dirs], set())

def recurse_rating(pt, expected):
    if (not pt.in_bounds(lines)) or int(lines[pt.y][pt.x]) != expected: return 0
    if expected == 9: return 1

    return sum([recurse_rating(pt + dir, expected + 1) for dir in dirs]) 

def part1():
    total = 0
    for y in range(len(lines)):
        for x in range(len(lines[y])):
            total += len(recurse_score(Point(y, x), 0))

    print(total)
    # Time: 21:26:32
    # Rating: 42000

def part2():
    total = 0
    for y in range(len(lines)):
        for x in range(len(lines[y])):
            total += recurse_rating(Point(y, x), 0)

    print(total)
    # Time: 21:33:42
    # Rating: 41002

if __name__ == '__main__':
    part1()
    part2()