'''https://adventofcode.com/2024/day/11'''

import utils

def update_cache(cache, key, value):
    cache[key] = value
    return value

def num_stones_split_from(stone, num_splits_left, cache):
    key = (stone, num_splits_left)

    if key in cache.keys(): return cache[key]
    if num_splits_left == 0: return update_cache(cache, key, 1)

    if stone == 0:
        return update_cache(cache, key, num_stones_split_from(1, num_splits_left - 1, cache))
    
    str_stone = str(stone)
    if len(str_stone) % 2 == 0:
        middle = len(str_stone)//2
        return update_cache(cache, key, num_stones_split_from(int(str_stone[:middle]), num_splits_left - 1, cache) + 
                                        num_stones_split_from(int(str_stone[middle:]), num_splits_left - 1, cache))
    
    return update_cache(cache, key, num_stones_split_from(2024 * stone, num_splits_left - 1, cache))

def start(num):
    line = utils.line_to_int_list(utils.get_lines('inputs/11.txt', True)[0])
    cache = dict()
    return sum([num_stones_split_from(stone, num, cache) for stone in line])

def part1():
    # Note: originally took brute force approach & got right answer; refactored after getting part 2
    print(start(25))
    # Time: 07:03:24
    # Rank: 28919

def part2():
    print(start(75))
    # Time: 21:45:36
    # Rank: 37781

if __name__ == '__main__':
    part1()
    part2()