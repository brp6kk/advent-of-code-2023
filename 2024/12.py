'''https://adventofcode.com/2024/day/12'''

import utils
from utils import Point
dirs = [Point(1, 0), Point(0, -1), Point(-1, 0), Point(0, 1)]

def find_region(current, plot, region, lines):
    if current in region or not current.in_bounds(lines) or lines[current.y][current.x] != plot: return
    region.add(current)
    for dir in dirs:
        find_region(current+dir, plot, region, lines)
    
def get_all_regions(lines):
    visited = set()
    regions = list()

    for y in range(len(lines)):
        for x in range(len(lines[y])):
            current = Point(y, x)
            if current in visited: continue
            region = set()
            find_region(current, lines[y][x], region, lines)
            regions.append(region)
            visited = visited.union(region)

    return regions

def part1():
    lines = utils.get_lines('inputs/12.txt', True)
    regions = get_all_regions(lines)

    total = 0
    for region in regions:
        # For each point within a region, add to the total perimeter how many neighbors are not within the region
        perimeter = sum([sum([point + dir not in region for dir in dirs]) for point in region])
        area = len(region)
        total += perimeter * area

    print(total)
    # Time: 18:17:53
    # Rank: 35250 

def part2():
    lines = utils.get_lines('inputs/12.txt', True)
    regions = get_all_regions(lines)

    total = 0
    for region in regions:
        sides = 0
        for point in region:
            top = point + Point(-1, 0)
            bottom = point + Point(1, 0)
            left = point + Point(0, -1)
            right = point + Point(0, 1)
            # Count the number of corners to get the number of sides
            sides += ((top not in region and left not in region) + 
                      (top not in region and right not in region) +
                      (bottom not in region and left not in region) + 
                      (bottom not in region and right not in region) + 
                      (top in region and right in region and top + Point(0, 1) not in region) +
                      (top in region and left in region and top + Point(0, -1) not in region) + 
                      (bottom in region and right in region and bottom + Point(0, 1) not in region) + 
                      (bottom in region and left in region and bottom + Point(0, -1) not in region)
            )
        area = len(region)
        total += sides * area

    print(total)
    # Time: 21:51:54
    # Rank: 25394

if __name__ == '__main__':
    part1()
    part2()