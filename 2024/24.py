'''https://adventofcode.com/2024/day/24'''

import utils
from enum import Enum

class Op(Enum):
    AND = 1
    OR = 2
    XOR = 3

def get_op(str_op):
    return {'AND': Op.AND, 'OR': Op.OR, 'XOR': Op.XOR}[str_op]

class Gate():
    def __init__(self, input_wire_1, op, input_wire_2, output_wire):
        self.input_wire_1 = input_wire_1 
        self.op = op
        self.input_wire_2 = input_wire_2
        self.output_wire = output_wire

    def can_apply(self, known_inputs):
        return self.input_wire_1 in known_inputs and self.input_wire_2 in known_inputs
    
    def apply(self, known_inputs):
        known_inputs[self.output_wire] = self.__getop()(known_inputs[self.input_wire_1], known_inputs[self.input_wire_2])

    def __getop(self):
        return {
            Op.AND: lambda x, y: x & y,
            Op.OR:  lambda x, y: x | y,
            Op.XOR: lambda x, y: x ^ y
        }[self.op]
    
    def __str__(self):
        return f"{self.input_wire_1} {self.op.name} {self.input_wire_2} -> {self.output_wire}"

def get_wires_gates():
    lines = utils.get_lines("inputs/24.txt", True)
    wires = dict()
    i = 0
    while lines[i] != '':
        w, v = lines[i].split(": ")
        wires[w] = int(v)
        i += 1
    gates = []
    for line in lines[i+1:]:
        i1, op, i2, _, out = line.split(" ")
        gates.append(Gate(i1, get_op(op), i2, out))
    return wires, gates

def part1():
    wires, gates = get_wires_gates()

    while len(gates) > 0:
        gate = gates.pop(0)
        if not gate.can_apply(wires):
            gates.append(gate)
            continue
        gate.apply(wires)

    str_bin = ''
    for z in sorted(filter(lambda x: x[0] == 'z', wires.keys())):
        str_bin = str(wires[z]) + str_bin

    print(int(str_bin, 2))
    # Time: 15:32:18
    # Rank: 17181

def part2():
    _, gates = get_wires_gates()

    edges = set()
    for g in gates:
        edges = edges.union({f"    {g.input_wire_1} --> {g.input_wire_1}{g.op.name}{g.input_wire_2}({g.op.name})\n",
                             f"    {g.input_wire_2} --> {g.input_wire_1}{g.op.name}{g.input_wire_2}({g.op.name})\n",
                             f"    {g.input_wire_1}{g.op.name}{g.input_wire_2} --> {g.output_wire}\n"})

    lines = ["graph TD\n"]
    lines.extend(edges)
    with open("outputs/24_mermaid_graph.txt", 'w') as fp:
        # Then copy-and-pasted graph into a mermaid visualization: https://mermaid.js.org/config/usage.html#simple-full-example
        # Found answer by manually inspecting graph for incorrect edges
        fp.writelines(lines)
    # Time: 17:30:21
    # Rank: 8373

if __name__ == "__main__":
    part1()
    part2()