'''https://adventofcode.com/2024/day/20'''

import utils
from utils import Point

WALL = '#'
START = 'S'
END = 'E'

LEFT = Point(0, -1)
RIGHT = Point(0, 1)
UP = Point(-1, 0)
DOWN = Point(1, 0)

def find_start(map):
    for y in range(len(map)):
        for x in range(len(map[y])):
            if map[y][x] == START:
                return Point(y, x)
    return None

def run_maze(map):
    '''Return dict where keys are points along race track, and dict[point] = time it takes to get to point if no cheating'''
    s = find_start(map)
    point_time_lookup = { s : 0 }
    last = Point(-1, -1)
    # Assumes there is one route with no loops, side streets, etc.
    while map[s.y][s.x] != END:
        for dir in [LEFT, RIGHT, UP, DOWN]:
            test = s + dir
            if test != last and map[test.y][test.x] != WALL: # Can't backtrack & can't enter walls
                point_time_lookup[test] = point_time_lookup[s] + 1
                last = s
                s = test
                break
    return point_time_lookup

def discover_cheats(start_loc, map, point_time_lookup, max_cheat_dist):
    '''For a given location, return list of (cheat endpoint, picosecond savings)'''
    endpoints = []
    for y in range(-max_cheat_dist, max_cheat_dist + 1):
        for x in range(-max_cheat_dist + abs(y), max_cheat_dist - abs(y) + 1):
            end_loc = start_loc + Point(y, x)
            if not end_loc.in_bounds(map) or map[end_loc.y][end_loc.x] == WALL: continue
            picoseconds_saved = point_time_lookup[end_loc] - point_time_lookup[start_loc] - abs(y) - abs(x)
            endpoints.append((end_loc, picoseconds_saved))
    return endpoints

def run(max_cheat_dist):
    lines = utils.get_lines("inputs/20.txt", True)
    point_time_lookup = run_maze(lines)

    return sum([len(list(filter(lambda x: x[1] >= 100, discover_cheats(pos, lines, point_time_lookup, max_cheat_dist)))) for pos in point_time_lookup.keys()]) 

def part1():
    print(run(2))
    # Time: 15:55:04
    # Rank: 18124

def part2():
    print(run(20))
    # Time: 19:37:34
    # Rank: 15465

if __name__ == '__main__':
    part1()
    part2()