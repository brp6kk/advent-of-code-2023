'''https://adventofcode.com/2024/day/8'''

import utils
from utils import Point

def get_antinodes_from_loc1(lines, loc1, loc2, limit):
    count = 0
    antinodes = set()
    diff = loc1 - loc2
    antinode = loc1 + diff
    while antinode.in_bounds(lines) and (not limit or count < limit):
        antinodes.add(antinode)
        count += 1
        antinode = antinode + diff
    return antinodes

def find_antinodes(limit=None):
    lines = utils.get_lines('inputs/08.txt', True)
    antennas = dict()
    for y in range(len(lines)):
        for x in range(len(lines[y])):
            val = lines[y][x]
            if val == '.': continue
            point = Point(y, x)
            if val in antennas.keys():
                antennas[val].append(point)
            else:
                antennas[val] = [point]
    
    antinodes = set()

    for freq in antennas.keys():
        locations = antennas[freq]
        for i in range(len(locations)):
            for j in range(i+1, len(locations)):
                antinodes = (antinodes.union(get_antinodes_from_loc1(lines, locations[i], locations[j], limit))
                                      .union(get_antinodes_from_loc1(lines, locations[j], locations[i], limit))
                                      .union(locations if limit is None else set()))

    return antinodes

def part1():
    print(len(find_antinodes(1)))
    # Time: 07:41:53
    # Rank: 26988

def part2():
    print(len(find_antinodes()))
    # Time: 11:47:14
    # Rank: 32657

if __name__ == '__main__':
    part1()
    part2()