'''https://adventofcode.com/2024/day/9'''

import utils

class File:
    def __init__(self, id, size):
        self.id = id
        self.size = size

    def __str__(self):
        return f"File with id {self.id} and size {self.size}"

def part1():
    line = utils.get_lines('inputs/09.txt', True)[0]
    is_file = True
    current_file_id = 0
    file_blocks = [] # simulates disk map, with each entry as one block
    free_space_locations = [] # indices of None in file_blocks
    for str_num in line:
        num = int(str_num)
        if is_file:
            file_blocks.extend([current_file_id] * num)
            current_file_id += 1
        else: 
            start = len(file_blocks) 
            free_space_locations.extend(range(start, num + start))
            file_blocks.extend([None] * num)
        is_file = not is_file

    # move from the right, putting non-None blocks in the left-most None spot
    rightmost_file_block_loc = len(file_blocks) - 1
    while free_space_locations[0] < rightmost_file_block_loc:
        if file_blocks[rightmost_file_block_loc] is not None: 
            new_loc = free_space_locations.pop(0)
            file_blocks[new_loc] = file_blocks[rightmost_file_block_loc]
            file_blocks[rightmost_file_block_loc] = None
        rightmost_file_block_loc -= 1

    total = 0
    for i in range(len(file_blocks)):
        if file_blocks[i] is None: break
        total += file_blocks[i] * i

    print(total)
    # Time: 07:16:01
    # Rank: 25351

def part2():
    line = utils.get_lines('inputs/09.txt', True)[0]
    is_file = True
    current_file_id = 0
    files = [] # simulates disk map, where each entry is one file (& chunks of contiguous free space count as a file)
    for str_num in line:
        num = int(str_num)
        file_id = current_file_id if is_file else None
        files.append(File(file_id, num))
        current_file_id += is_file
        is_file = not is_file

    # move from the right, putting non-None files in the left-most None spot that fits (if exists)
    rightmost_file_block_loc = len(files) - 1
    while rightmost_file_block_loc > 0:
        if files[rightmost_file_block_loc].id is None: 
            rightmost_file_block_loc -= 1
            continue

        # move from the left, to find the None spot that file can be put in
        for i in range(rightmost_file_block_loc):
            if files[i].id is not None or files[i].size < files[rightmost_file_block_loc].size: continue

            files[i].id = files[rightmost_file_block_loc].id
            # split None spot to preserve free space not taken up by file
            if files[i].size != files[rightmost_file_block_loc].size:
                files.insert(i+1, File(None, files[i].size - files[rightmost_file_block_loc].size))
                rightmost_file_block_loc += 1
                files[i].size = files[rightmost_file_block_loc].size

            # original file location is now None. Cleanup by merging with left/right if possible
            files[rightmost_file_block_loc].id = None
            if rightmost_file_block_loc < len(files) - 2 and files[rightmost_file_block_loc+1].id is None:
                files[rightmost_file_block_loc].size += files[rightmost_file_block_loc+1].size
                files.pop(rightmost_file_block_loc+1)
            if files[rightmost_file_block_loc-1].id is None:
                files[rightmost_file_block_loc-1].size += files[rightmost_file_block_loc].size
                files.pop(rightmost_file_block_loc)
            break

        rightmost_file_block_loc -= 1

    i = 0
    total = 0
    for f in files:
        for _ in range(f.size):
            if f.id is not None:
                total += i * f.id
            i += 1

    print(total)
    # Time: 22:02:22
    # Rank: 36495

if __name__ == '__main__':
    part1()
    part2()