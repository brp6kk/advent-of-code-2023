'''https://adventofcode.com/2024/day/5'''

import utils

def build_lookup(lines):
    i = 0
    lookup = dict()
    while lines[i].strip() != '':
        before, after = [int(x) for x in lines[i].split('|')]
        if before in lookup.keys():
            lookup[before].add(after)
        else:
            lookup[before] = {after}
        i += 1
    return lookup, i

def is_update_valid(update, lookup):
    for i in range(len(update)):
        required_afters = lookup[update[i]]
        actual_befores = set(update[:i])
        if len(required_afters.intersection(actual_befores)) > 0:
            return False
    return True

def part1():
    lines = utils.get_lines('inputs/05.txt')

    lookup, i = build_lookup(lines)    
    total = 0
    for update_str in lines[i+1:]:
        update = utils.line_to_int_list(update_str, ',')
        if is_update_valid(update, lookup):
            total += update[int((len(update) - 1) / 2)]

    print(total)
    # Time: 07:12:13
    # Rank: 37783

def part2():
    lines = utils.get_lines('inputs/05.txt')

    lookup, i = build_lookup(lines)
    total = 0
    for update_str in lines[i+1:]:
        update = utils.line_to_int_list(update_str, ',')
        if is_update_valid(update, lookup): continue

        # Bubble sort
        is_sorted = False
        while not is_sorted:
            is_sorted = True
            for j in range(len(update)-1):
                before, after = update[j], update[j+1]
                if after in lookup.keys() and before in lookup[after]:
                    is_sorted = False
                    update[j] = after
                    update[j+1] = before
        total += update[int((len(update) - 1) / 2)]

    print(total)
    # Time: 07:38:05
    # Rank: 30354

if __name__ == '__main__':
    part1()
    part2()