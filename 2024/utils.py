class Point:
    def __init__(self, y, x):
        self.y = y
        self.x = x
    
    def __eq__(self, other):
        return isinstance(other, Point) and other.y == self.y and other.x == self.x
    
    def __add__(self, other):
        return Point(other.y + self.y, other.x + self.x)
    
    def __sub__(self, other):
        return Point(self.y - other.y, self.x - other.x)
    
    def __mul__(self, other):
        return Point(self.y * other.y, self.x * other.x)
    
    def __str__(self):
        return f"({self.y}, {self.x})"
    
    def __hash__(self):
        return hash(str(self))
    
    def in_bounds(self, grid):
        return self.y >= 0 and self.x >= 0 and self.y < len(grid) and self.x < len(grid[0])

def get_lines(fname, should_clean=False):
    with open(fname, 'r') as fp:
        lines = fp.readlines()
    if should_clean:
        lines = [x.rstrip() for x in lines]
    return lines

def line_to_int_list(line, delim=None):
    if delim:
        return [int(x) for x in line.split(delim)]
    return [int(x) for x in line.split()]    