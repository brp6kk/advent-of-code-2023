'''https://adventofcode.com/2024/day/18'''

import utils
import heapq

MAP_SIZE = 70

def dijkstra(corrupted):
    ''' 
    Run modified Dijkstra's algorithm.
    Edges exist between points on the grid with one of (y, x) same and the other off by one.
    '''
    # Set up data structures
    # visited = [(Location y, location x)] 
    visited = {
        (0, 0)
    }
    # (total score of path to location, location)
    heap = [
        (1, (1, 0)),
        (1, (0, 1))
    ] 
    heapq.heapify(heap)

    min_val = -1

    while len(heap) > 0:
        dist, (y, x) = heapq.heappop(heap)

        if ((y, x) in visited or (y, x) in corrupted):
            # Shorter path previously found, or cannot enter this spot
            continue
        if (y == MAP_SIZE and x == MAP_SIZE):
            # Reached endpoint
            min_val = dist
            break

        visited.add((y, x))
        
        for dy, dx in [(0, 1), (0, -1), (1, 0), (-1, 0)]:
            y_next = y + dy
            x_next = x + dx

            if y_next < 0 or y_next > MAP_SIZE or x_next < 0 or x_next > MAP_SIZE:
                continue

            # Add score to total & push to heap
            heapq.heappush(heap, (dist + 1, ((y_next, x_next))))
            
    return min_val

def part1():
    corrupted = set([(int(l[1]), int(l[0])) for l in [x.split(",") for x in utils.get_lines("inputs/18.txt")]][:1024])
    print(dijkstra(corrupted))
    # Time: 06:34:20
    # Rank: 13931

def part2():
    corrupted = [(int(l[1]), int(l[0])) for l in [x.split(",") for x in utils.get_lines("inputs/18.txt")]]
    for i in range(1024,3450):
        if dijkstra(set(corrupted[:i])) == -1:
            print(corrupted[i-1])
            break
    # Time: 06:47:09
    # Rank: 13219

if __name__ == '__main__':
    part1()
    part2()