'''https://adventofcode.com/2024/day/21'''

import utils
from utils import Point

A = 'A'
L = Point(0, -1)
R = Point(0, 1)
U = Point(-1, 0)
D = Point(1, 0)

NUMERIC_KEYPAD = { 
    '7': Point(0, 0), '8': Point(0, 1), '9': Point(0, 2), 
    '4': Point(1, 0), '5': Point(1, 1), '6': Point(1, 2), 
    '1': Point(2, 0), '2': Point(2, 1), '3': Point(2, 2),
                      '0': Point(3, 1),   A: Point(3, 2)
}
DIRECTIONAL_KEYPAD = {
                    U: Point(0, 1), A: Point(0, 2),
    L: Point(1, 0), D: Point(1, 1), R: Point(1, 2)
}

def get_movements_for(last_position, current_position, is_numeric):
    individual_movements = []
    total_movement = current_position - last_position
    x_movement = Point(0, 0 if total_movement.x == 0 else int(total_movement.x / abs(total_movement.x)))
    y_movement = Point(0 if total_movement.y == 0 else int(total_movement.y / abs(total_movement.y)), 0)
    individual_x_movements = abs(total_movement.x) * [x_movement]
    individual_y_movements = abs(total_movement.y) * [y_movement]
        
    # Prioritize, if possible, going to left, then middle, then right
    if ((last_position.x == 0 and not ((is_numeric and current_position.y != 3) or 
                                          (not is_numeric and current_position.y != 0))) or
          (x_movement == L and not ((is_numeric and last_position.y == 3 and current_position.x == 0) or 
                                    (not is_numeric and last_position.y == 0 and current_position.x == 0)))):
        individual_movements.extend(individual_x_movements)
        individual_movements.extend(individual_y_movements)
    else:
        individual_movements.extend(individual_y_movements)
        individual_movements.extend(individual_x_movements)
    individual_movements.append(A)

    return individual_movements

def get_numeric_seq(code):
    last_char = A
    movements = []

    for current_char in code:
        individual_movements = get_movements_for(NUMERIC_KEYPAD[last_char],  NUMERIC_KEYPAD[current_char], True)
        movements.append(tuple(individual_movements))
        last_char = current_char

    return movements

def get_directional_seq_recursive(code, direction_recursion_count, cache):
    key = (code, direction_recursion_count)
    if key in cache.keys(): return cache[key]

    count = 0
    last_char = A
    for current_char in code: 
        movements = get_movements_for(DIRECTIONAL_KEYPAD[last_char], DIRECTIONAL_KEYPAD[current_char], False)
        count += len(movements) if direction_recursion_count == 1 else get_directional_seq_recursive(tuple(movements), direction_recursion_count - 1, cache) 
        last_char = current_char

    cache[key] = count 
    return count

def run(direction_recursion_count):
    lines = utils.get_lines("inputs/21.txt", True)
    total = 0
    cache = dict()
    for code in lines:
        count_presses = sum([get_directional_seq_recursive(x, direction_recursion_count, cache) for x in get_numeric_seq(code)])
        total += count_presses * int(code[:3])
    print(total)

def part1():
    run(2)
    # Time: 17:40:53
    # Rank: 10904

def part2():
    run(25)
    # Time: >24h
    # Rank: 13867

if __name__ == '__main__':
    part1()
    part2()