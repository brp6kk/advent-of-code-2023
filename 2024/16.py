'''https://adventofcode.com/2024/day/16'''

import utils
import heapq

def dijkstra(find_all_shortest_paths = False):
    ''' 
    Run modified Dijkstra's algorithm.
    Edges exist between points on the grid with one of (y, x) same and the other off by one.
    '''
    map = utils.get_lines("inputs/16.txt", True)

    # Set up data structures
    # visited[((Location y, location x), (change in x to reach location, change in y to reach location))] = min_score
    visited = dict() 
    # (total score of path to location, (location, (change in y, x), set of points in path))
    heap = [
        (1000 + 1, ((len(map) - 3, 1), (-1, 0), {(len(map) - 2,1)})), 
        (1, ((len(map) - 2, 2), (0, 1), {(len(map) - 2,1)}))
    ] 
    heapq.heapify(heap)

    min_val = -1
    points_in_min_paths = set()

    while len(heap) > 0:
        score, ((y, x), (dy, dx), path_points) = heapq.heappop(heap)

        if (((y, x), (dy, dx)) in visited.keys() and visited[((y, x), (dy, dx))] < score):
            # Shorter path previously found
            continue
        path_points.add((y, x))
        if (y == 1 and x == len(map[0]) - 2 and (min_val == -1 or min_val == score)):
            # Reached endpoint
            min_val = score
            points_in_min_paths = points_in_min_paths.union(path_points)
            if not find_all_shortest_paths:
                break
            continue
        elif y == 1 and x == len(map[0]) - 2 and min_val < score:
            # All min-length paths found
            break

        visited[((y, x), (dy, dx))] = score
        
        # Can't move backwards
        possible_turns = [(0, 1), (0, -1), (1, 0), (-1, 0)]
        possible_turns.remove((-dy, -dx))

        for dy_next, dx_next in possible_turns:
            y_next = y + dy_next
            x_next = x + dx_next
            if map[y][x] == '#':
                continue

            # Add score to total & push to heap
            score_next = score + 1 + (1000 * (dy_next != dy or dx_next != dx))
            path_points_copy = path_points.copy()
            heapq.heappush(heap, (score_next, ((y_next, x_next), (dy_next, dx_next), path_points_copy)))
            
    return min_val, len(points_in_min_paths)

def part1():
    print(dijkstra()[0])
    # Time: 09:27:37
    # Rank: 14472

def part2():
    print(dijkstra(True)[1])
    # Time: 10:21:32
    # Rank: 10479

if __name__ == '__main__':
    part1()
    part2()