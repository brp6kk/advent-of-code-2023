'''https://adventofcode.com/2024/day/17'''

import utils

class Registers:
    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c

    def __str__(self):
        return f"a = {self.a}; b = {self.b}; c = {self.c}"
    
class Computer:
    def __init__(self, reg):
        self.reg = reg
        self.program = []
        self.pc = 0
        self.out = []

    def run_program(self, program):
        self.program = program
        self.pc = 0
        self.out = []
        while self.__execute_next_instruction(): continue

    def __execute_next_instruction(self):
        if not self.__has_next_instruction(): return False
        [self.__adv, self.__bxl, self.__bst, self.__jnz, self.__bxc, self.__out, self.__bdv, self.__cdv][self.program[self.pc]](self.program[self.pc+1])
        self.pc += 2
        return True

    def __has_next_instruction(self):
        return self.pc + 1 < len(self.program)
    
    def __adv(self, op):
        self.reg.a = self.reg.a // 2 ** self.__combo(op)

    def __bxl(self, op):
        self.reg.b = self.reg.b ^ op

    def __bst(self, op):
        self.reg.b = self.__combo(op) % 8

    def __jnz(self, op):
        if self.reg.a == 0: return
        self.pc = op - 2

    def __bxc(self, _):
        self.reg.b = self.reg.b ^ self.reg.c
    
    def __out(self, op):
        self.out.append(self.__combo(op) % 8)
    
    def __bdv(self, op):
        self.reg.b = self.reg.a // 2 ** self.__combo(op)

    def __cdv(self, op):
        self.reg.c = self.reg.a // 2 ** self.__combo(op)

    def __combo(self, op):
        return [0, 1, 2, 3, self.reg.a, self.reg.b, self.reg.c][op]

def find_quine(start, exp, program):
    results = [1000000000000000000]
    if exp == -1: return start
    for i in range(8):
        check_num = start + i * 8**exp
        cmp = Computer(Registers(check_num, 0, 0))
        cmp.run_program(program)
        if len(cmp.out) != len(program): continue
        if cmp.out[exp] == program[exp]: results.append(find_quine(check_num, exp - 1, program))
    
    return min(results)

def part1():
    lines = utils.get_lines('inputs/17.txt', True)
    reg = [int(x.split(": ")[1]) for x in lines[:3]]
    reg = Registers(reg[0], reg[1], reg[2])
    program = utils.line_to_int_list(lines[4].split()[1], ",")    

    cmp = Computer(reg)
    cmp.run_program(program)

    print(",".join([str(x) for x in cmp.out]))
    # Time: 09:37:44
    # Rank: 17292

def part2():
    lines = utils.get_lines('inputs/17.txt', True)
    program = utils.line_to_int_list(lines[4].split()[1], ",") 

    print(find_quine(0, 15, program))
    # Time: 11:21:47
    # Rank: 8880

if __name__ == '__main__':
    part1()
    part2()